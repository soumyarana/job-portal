const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

const auth=require('../auth/auth-admin'); // Get Auth Function
const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url

// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser 

// Include models and controllers
const Model = require('../model/adminModel');     // include model
const reqCompaniesController = require('../controllers/requested-companies');  // include controller

// Image upload
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});


//all job applications routes


  //get category list
  app.get('/all',auth.authentic, auth.deleteUser, function(req, res){
    reqCompaniesController.getAllApplications(req, res, function(err, result){
      res.render('pages/requested-companies/requested-companies-list',{title: 'All applications List', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })


  //applicant view details
  app.get('/view',auth.authentic, auth.deleteUser, function(req, res){
    console.log("route........")
    console.log(req.query.company_id)

    reqCompaniesController.viewApplicant(req, res, function(err, result){
      res.render('pages/requested-companies/applicant-details',{title: 'Applicant Details', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })

  //candidate interview selection email

  app.get('/approved',auth.checkMemberLogin,urlencodedParser,function(req,res) {
    console.log("55555555"+req.query.companyEmail)
    const configRoute = require('../config/route'); // Get config rotue
    const connectDb = require('../config/database');
    let databaseName = configRoute.databaseName; // Store Database Name
    let collectionName = "companiesCollection"; // Define Collection Name
    
    
  
    //Check email Unique
    connectDb(function (err, client) {
          if (err) {
            throw err;
          } else {
            const db = client.db(databaseName); // Enter Database Name
            //console.log(req.body.otp);
            //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
            db.collection(collectionName).findOneAndUpdate({
              "company_id": parseInt(req.query.companyId)
            },{
              $set:{
                "companyAuthenticated" : true,
                "companyRejected" : false,
                "rejectionCause" : null
              }
            },{
              upsert:true
            }, function (err, result) {

              client.close();
              if (!result) {
                var error = {
                  param: "otp",
                  msg: "Your OTP does not Match",
                  value: req.body.otp
                };
                if (!errors) {
                  errors = [];
                }
                errors.push(error);
              } else {
                var nodemailer = require('nodemailer');
                var smtpTransport = require('nodemailer-smtp-transport');
  
                var mailAccountUser = 'testsmtptt@gmail.com'
                var mailAccountPassword = 'testsmtppassword'
                //var mailAccountPassword =process.env.GMAIL_PASS
  
                var fromEmailAddress = 'testsmtptt@gmail.com'
                var toEmailAddress = req.query.companyEmail
  
                var transport = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: mailAccountUser,
                        pass: mailAccountPassword
                    }
                })
  
                var mail = {
                    from: fromEmailAddress,
                    to: toEmailAddress,
                    subject: "Selection Mail",
                    text: "Job Portal",
                    html: "Your requested company is approved by Job Portal"
                }
  
                transport.sendMail(mail, function(error, response){
                    if(error){
                        console.log(error);
                    }else{
                        console.log("Message sent: ");
                        //console.log(response);
                    }
  
                    transport.close();
                })
                
                // callback(null, result);
                result = []
                result.push({
                  msg: "Email has send to candidate email id"
                });
                req.session.errors = false;
                req.session.success = result;
                
                res.redirect('/admin/requested-companies/all');

              }
            })
                // Here need  to be next part of action - not outside find callback!
            // cb(errors)
          }
      })
  
      })


  // app.post('/rejected',auth.checkMemberLogin,function(req,res) {
  //       console.log(req.body.rejectionCause);
  //       console.log(req.query.companyEmail);
  //       const configRoute = require('../config/route'); // Get config rotue
  //       const connectDb = require('../config/database');
  //       let databaseName = configRoute.databaseName; // Store Database Name
  //       let collectionName = "companiesCollection"; // Define Collection Name

  //       //Check email Unique
  //       connectDb(function (err, client) {
  //             if (err) {
  //               throw err;
  //             } else {
  //               const db = client.db(databaseName); // Enter Database Name
  //               //console.log(req.body.otp);
  //               //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
  //               db.collection(collectionName).findOneAndUpdate({
  //                 "companyEmail": req.query.companyEmail
  //               },{
  //                 $set:{
  //                   "companyRejected" : true,
  //                   "rejectionCause" : req.body.rejectionCause                
  //                 }
  //               },{
  //                 upsert:true
  //               }, function (err, result) {
  //                 console.log("/////....",result)
  //                 client.close();
  //                 if (!result) {
  //                   var error = {
  //                     param: "otp",
  //                     msg: "Your OTP does not Match",
  //                     value: req.body.otp
  //                   };
  //                   if (!errors) {
  //                     errors = [];
  //                   }
  //                   errors.push(error);
  //                 } else {
  //                   var nodemailer = require('nodemailer');
  //                   var smtpTransport = require('nodemailer-smtp-transport');
      
  //                   var mailAccountUser = 'testsmtptt@gmail.com'
  //                   var mailAccountPassword = 'testsmtppassword'
  //                   //var mailAccountPassword =process.env.GMAIL_PASS
      
  //                   var fromEmailAddress = 'testsmtptt@gmail.com'
  //                   var toEmailAddress = req.query.companyEmail
      
  //                   var transport = nodemailer.createTransport({
  //                       service: 'gmail',
  //                       auth: {
  //                           user: mailAccountUser,
  //                           pass: mailAccountPassword
  //                       }
  //                   })
      
  //                   var mail = {
  //                       from: fromEmailAddress,
  //                       to: toEmailAddress,
  //                       subject: "Selection Mail",
  //                       text: "Job Portal",
  //                       html: "Your requested company is approved by Job Portal"
  //                   }
      
  //                   transport.sendMail(mail, function(error, response){
  //                       if(error){
  //                           console.log(error);
  //                       }else{
  //                           console.log("Message sent: ");
  //                           //console.log(response);
  //                       }
      
  //                       transport.close();
  //                   })
                    
  //                   // callback(null, result);
  //                   result = []
  //                   result.push({
  //                     msg: "Email has send to candidate email id"
  //                   });
  //                   req.session.errors = false;
  //                   req.session.success = result;
                    
  //                   res.redirect('/admin/requested-companies/all');
    
  //                 }
  //               })
  //                   // Here need  to be next part of action - not outside find callback!
  //               // cb(errors)
  //             }
  //         })
      
  //         })

  app.post('/rejected',auth.authentic, auth.deleteUser,urlencodedParser,reqCompaniesController.rejectedCompany )        


  module.exports=app;
  
 