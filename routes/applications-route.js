const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

const auth=require('../auth/auth'); // Get Auth Function
const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url

// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser 

// Include models and controllers
const Model = require('../model/recruiterModel');     // include model
const applicationsController = require('../controllers/job-application');  // include controller
const { callbackPromise } = require('nodemailer/lib/shared');

// Image upload
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});


//all job applications routes


  //get category list
  app.get('/all',auth.authentic, auth.deleteUser, function(req, res){
    applicationsController.getAllApplications(req, res, function(err, result){
      res.render('pages/job-application/application-list',{title: 'All applications List', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })

  //specific company applications
  app.get('/company/all',auth.authentic, auth.deleteUser, function(req, res){
    applicationsController.getCompanyApplications(req, res, function(err, result){
      res.render('pages/job-application/application-list',{title: 'All applications List', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })


  //applicant view details
  app.get('/view',auth.authentic, auth.deleteUser, function(req, res){
    console.log("route........")
    console.log(req.query.applicant_id)

    applicationsController.viewApplicant(req, res, function(err, result){
      res.render('pages/job-application/applicant-details',{title: 'Applicant Details', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })

  //candidate interview selection email

  app.get('/selection-mail',auth.authentic,auth.deleteUser,function(req,res) {

    console.log("////.........................",req.query.applicant_email);
    console.log(req.query.applicant_id);
    console.log(req.query.company_name);
    const configRoute = require('../config/route'); // Get config rotue
    const connectDb = require('../config/database');
    const collectionIndex=require('../model/collectionIndex');
    let date = require('date-and-time');
    let databaseName = configRoute.databaseName; // Store Database Name
    let collectionName = "memberNotification"; // Define Collection Name
  
  
      connectDb(function (err, client) {
        if (err) {
          throw err;
        } else {
          const db = client.db(databaseName); // Enter Database Name
          //console.log(req.body.otp);
          //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
          db.collection("jobPostCollection").findOneAndUpdate({
            "jobPost_id": parseInt(req.query.jobPost_id)
          },{
            $set: {
              'applications.$[elem].notSelected': false,
              'saveJobs.$[elem].notSelected': false,
            }
          },{
            arrayFilters: [ {"elem.applicant_id": parseInt(req.query.applicant_id)}]
         }, function (err, result) {

            //console.log(result);
            if (!result) {
              var error = {
                param: "otp",
                msg: "Your OTP does not Match",
                value: req.body.otp
              };
              if (!errors) {
                errors = [];
              }
              errors.push(error);
            } else {
              db.collection("memberHistoryCollection").updateMany({
                "jobPost_id": req.query.jobPost_id,"member_id":parseInt(req.query.applicant_id)
              },{
                $set:{
                  "notSelected" : false
                }
              },
              function(err,histryEdit){
                if(err) {
                  throw err;
                }else{
                  let message = "You are selected for the interview at "+req.query.company_name;
              req.collectionName=collectionName;
              collectionIndex.checkIndex(req, res, function(result){

                  connectDb(function(err, client){
                  if(err){
                      throw err;
                  }else{
                      let now = new Date();
                      var item={
                          memberNotification_id:result,
                          member_id:req.query.applicant_id,
                          memberEmail: req.query.applicant_email,
                          message:message,
                          notificationNotReaded:true,      
                          created_ip_address:req.connection.remoteAddress,
                          modified_ip_address:req.connection.remoteAddress,
                          created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
                          modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
                      }
                      
                      
                      const db=client.db(databaseName); // Enter Database Name
                              db.collection(collectionName).insertOne(item, function(err,result){
                                  
                                  if(err){
                                      throw err;
                                  }else{
                                    var nodemailer = require('nodemailer');
                                    var smtpTransport = require('nodemailer-smtp-transport');
                      
                                    var mailAccountUser = 'testsmtptt@gmail.com'
                                    var mailAccountPassword = 'testsmtppassword'
                                    //var mailAccountPassword =process.env.GMAIL_PASS
                      
                                    var fromEmailAddress = 'testsmtptt@gmail.com'
                                    var toEmailAddress = req.query.applicant_email
                      
                                    var transport = nodemailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: mailAccountUser,
                                            pass: mailAccountPassword
                                        }
                                    })
                      
                                    var mail = {
                                        from: fromEmailAddress,
                                        to: toEmailAddress,
                                        subject: "Interview Mail",
                                        text: "Job Portal",
                                        html: "You are selected for the interview by "+req.query.company_name
                                    }
                      
                                    transport.sendMail(mail, function(error, response){
                                        if(error){
                                            console.log(error);
                                        }else{
                                            console.log("Message sent: ");
                                            //console.log(response);
                                        }
                      
                                        transport.close();
                                    })
                                    
                                    // callback(null, result);
                                    result = []
                                    result.push({
                                      msg: "Email has send to candidate email id"
                                    });
                                    req.session.errors = false;
                                    req.session.success = result;
                                    
                                    res.redirect('/recruiter/job-applications/all');
                      
                                  }
                                  //client.close();
                              })
                      }       
                }) 
                       
              }) 
              }
              })
            }
          })
          // Here need  to be next part of action - not outside find callback!
          // cb(errors)
        }
      })







      //         }
      //       })
      //           // Here need  to be next part of action - not outside find callback!
      //       // cb(errors)
      //     }
      // })
  
      })





  module.exports=app;
  
 