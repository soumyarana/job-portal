const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

const auth=require('../auth/auth'); // Get Auth Function
const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url

// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser 

// Include models and controllers
const Model = require('../model/recruiterModel');     // include model
const jobPostControllers = require('../controllers/jobPosts');  // include controller

// Image upload
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});


//all job post routes


  app.get('/addNew',auth.authentic, auth.deleteUser, function(req, res){
    // res.render('pages/jobPost/addNewJob',{title: 'Add-New', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', baseUrl:baseUrl});
    // req.session.errors= null; req.session.success=null;

    jobPostControllers.getJobPostedWithCategory(req, res, function(err, result){
      res.render('pages/jobPost/addNewJob',{title: 'Add-New', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin',companies:result.company,categories:result.category, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
  })
  //homeControllers.basicCompanyDetails ,
  app.post('/addNew',auth.authentic, auth.deleteUser,urlencodedParser,jobPostControllers.addNewJob )

  app.get('/job-list',auth.authentic, auth.deleteUser, function(req, res){
    

    jobPostControllers.getJobPosted(req, res, function(err, result,dropDownData){
      res.render('pages/jobPost/jobList',{title: 'Job list', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result,dropDownData:dropDownData, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })

  app.post('/job-list',auth.authentic, auth.deleteUser,urlencodedParser,jobPostControllers.sortByData )

  app.get('/companies',auth.authentic, auth.deleteUser, function(req, res){
    

    jobPostControllers.getCompanies(req, res, function(err, result){
      res.render('pages/jobPost/myCompanies',{title: 'Job list', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result,baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })



  module.exports=app;
  
 