const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

const auth=require('../auth/auth'); // Get Auth Function
const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url

// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser   
// app.use(expressSession({        // Define Express Session
//     secret: 'secretkey',
//     saveUninitialized: true,
//     resave: true
// }));

// Include models and controllers
const Model = require('../model/adminModel');     // include model
const pageControllers = require('../controllers/pages');  // include controller

// Image upload
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});


// Journal Rotues

// Load Add page
app.get('/add',auth.authentic, auth.deleteUser, function(req, res){
    res.render('pages/page/add',{title: 'Add',sessionData:req.session,  success:req.session.success, errors:req.session.errors, layout: 'admin', baseUrl:baseUrl});
     req.session.errors= null; req.session.success=null;
})
// Fetch Add Details
 app.post('/add',auth.authentic, auth.deleteUser,urlencodedParser, pageControllers.add)


// Load lising Page
app.get('/listing',auth.authentic, auth.deleteUser, function(req, res){
    pageControllers.getPages(req, res, function(err, result){
        res.render('pages/page/listing',{title: 'Listing', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
         req.session.errors= null; req.session.success=null;
    });
})

// Delete Request
app.get('/delete',auth.authentic, auth.deleteUser, urlencodedParser, pageControllers.delete)


// Load Edit Page
app.get('/edit',auth.authentic, auth.deleteUser, function(req, res){
    pageControllers.getPage(req, res, function(err, result){
        res.render('pages/page/edit',{title: 'Edit', success:req.session.success, errors:req.session.errors, layout: 'admin', sessionData:req.session, result:result, baseUrl:baseUrl});
         req.session.errors= null; req.session.success=null;
    });
})


// Fetch Edit Details
app.post('/edit',auth.authentic, auth.deleteUser, [urlencodedParser,upload.single('image') ], pageControllers.edit)

// Updated Status
app.get('/status',auth.authentic, auth.deleteUser, urlencodedParser, pageControllers.status)

module.exports=app;
