const express = require('express');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({
  extended: false
});

const auth = require('../auth/auth'); // Get Auth Function
const configRoute = require('../config/route'); // Get config rotue
var baseUrl = configRoute.baseUrl; // Get Base Url
// Use app
const app = express.Router(); //Get App
app.use(expressValidator()) //Define Express Validator
app.use(bodyParser.json()); // Define Bodyparser   
// app.use(expressSession({        // Define Express Session
//     secret: 'secretkey',
//     saveUninitialized: true,
//     resave: true
// }));

// Include models and controllers
const Model = require('../model/recruiterModel'); // include model
const recruiterControllers = require('../controllers/recruiters'); // include controller

// Image upload
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/images/uploads')
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now() + '.jpg')
  }
});
var upload = multer({
  storage: storage
});


// Routes

// Load Registation page
app.get('/registration', function (req, res) {
  res.render('pages/recruiter/registration_old', {
    title: 'Registration',
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'login',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})
// Fetch Registration Details
app.post('/registration', [urlencodedParser, upload.single('image')], recruiterControllers.registration)


//email verification in registration

app.get('/registration/:token', function (req, res) {
  console.log(req.params.token);
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  let collectionName = "userdata"; // Define Collection Name

  // Check email Unique
  connectDb(function (err, client) {
    if (err) {
      throw err;
    } else {
      const db = client.db(databaseName); // Enter Database Name
      //console.log(req.body.otp);
      //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
      db.collection(collectionName).findOneAndUpdate({
        "token": req.params.token
      }, {
        $set: {
          emailVerified: "true"
        }
      }, function (err, result) {
        client.close();
        if (!result) {
          var error = {
            param: "otp",
            msg: "Your OTP does not Match",
            value: req.body.otp
          };
          if (!errors) {
            errors = [];
          }
          errors.push(error);
        } else {
          result = []
          result.push({
            msg: "Email verified.."
          });
          req.session.errors = false;
          req.session.success = result;
          req.session.emailNotVerified = false;
          console.log("email in..")
          res.redirect('/recruiter/login');
          //console.log("result ",result)
          //console.log("result email ",result.email)

          //set email verified true in collection

        }
      })
      // Here need  to be next part of action - not outside find callback!
      // cb(errors)
    }
  })

})


// Load Login Page
app.get('/login', auth.checkLogin, function (req, res) {
  res.render('pages/recruiter/login', {
    title: 'Login',
    sessionData: req.session,
    success: req.session.success,
    errors: req.session.errors,
    reverify: req.session.emailNotVerified,
    loginEmail: req.session.loginEmail,
    layout: 'login',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})
// Fetch Login Details
app.post('/login', urlencodedParser, auth.emailVerifiedrecruiter, recruiterControllers.login)

//Resend reverification email
app.get('/login/:email', auth.checkMemberLogin, function (req, res) {

  console.log(req.params.email);
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "userdata"; // Define Collection Name


  let token = cryptoRandomString({
    length: 25,
    type: 'hex'
  })

  let urlToMail = `http://localhost:5000/recruiter/registration/${token}`;



  // Check email Unique
  connectDb(function (err, client) {
    if (err) {
      throw err;
    } else {
      const db = client.db(databaseName); // Enter Database Name
      //console.log(req.body.otp);
      //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
      db.collection(collectionName).findOneAndUpdate({
        "email": req.params.email
      }, {
        $set: {
          "token": token
        }
      }, function (err, result) {
        console.log(result);
        client.close();
        if (!result) {
          var error = {
            param: "otp",
            msg: "Your OTP does not Match",
            value: req.body.otp
          };
          if (!errors) {
            errors = [];
          }
          errors.push(error);
        } else {
          var nodemailer = require('nodemailer');
          var smtpTransport = require('nodemailer-smtp-transport');

          var mailAccountUser = 'testsmtptt@gmail.com'
          var mailAccountPassword = 'testsmtppassword'
          //var mailAccountPassword =process.env.GMAIL_PASS

          var fromEmailAddress = 'testsmtptt@gmail.com'
          var toEmailAddress = req.params.email

          var transport = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: mailAccountUser,
              pass: mailAccountPassword
            }
          })

          var mail = {
            from: fromEmailAddress,
            to: toEmailAddress,
            subject: "Email Verify",
            text: "Job Portal",
            html: `Please click the link to verify the email:<a href = "${urlToMail}">login link click here...</a>`
          }

          transport.sendMail(mail, function (error, response) {
            if (error) {
              console.log(error);
            } else {
              console.log("Message sent: ");
              //console.log(response);
            }

            transport.close();
          })

          // callback(null, result);
          result = []
          result.push({
            msg: "Your Login link has Send to Your Email address"
          });
          req.session.errors = false;
          req.session.success = result;

          res.redirect('/recruiter/login');
        }
      })
      // Here need  to be next part of action - not outside find callback!
      // cb(errors)
    }
  })

})


// Load Profile Page
app.get('/profile', auth.authentic, auth.deleteUser, function (req, res) {
  console.log("redirect profile")
  recruiterControllers.getProfile(req, res, function (err, result) {
    res.render('pages/recruiter/profile', {
      title: 'Profile',
      success: req.session.success,
      errors: req.session.errors,
      layout: 'recruiter',
      sessionData: req.session,
      result: result,
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})
// Fetch Profile Details
app.post('/profile', auth.authentic, auth.deleteUser, [urlencodedParser, upload.single('image')], recruiterControllers.profile)

app.get('/notifications/:user_id',auth.authentic, auth.deleteUser,function(req,res) {
  console.log("from client...",req.params.user_id);
  let user_id = req.params.user_id;
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "userdata"; // Define Collection Name


  connectDb(function(err, client){
    if(err){
        throw err;
    }else{
        const db=client.db(databaseName); // Enter Database Name
        db.collection(collectionName).findOne({"user_id":parseInt(user_id)}, function (err, memberData) {
          
            client.close();
            if(err){
                callback(err);
            }else{     
                if(memberData){ 
                  if(memberData.applicationNotifications){
                    let memberArray = memberData.applicationNotifications
                    let unreadData = memberArray.filter((elem) => elem.readNotification == false)
                  
                    res.send(unreadData);
                  }else{
                    let arr = [];
                    res.send(arr)
                  }                  
                    //callback(null, memberData);                        
                }else{
                    //callback(null);
                }
            }
        });
    }                
});




})

app.get('/notifications/company/:user_id',auth.authentic, auth.deleteUser,function(req,res) {
  console.log("from client...",req.params.user_id);
  let user_id = req.params.user_id;
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "userdata"; // Define Collection Name


  connectDb(function(err, client){
    if(err){
        throw err;
    }else{
        const db=client.db(databaseName); // Enter Database Name
        db.collection(collectionName).findOne({"user_id":parseInt(user_id)}, function (err, memberData) {
          
            client.close();
            if(err){
                callback(err);
            }else{     
                if(memberData){ 
                  if(memberData.applicationNotifications){
                    let memberArray = memberData.applicationNotifications
                    let unreadData = memberArray.filter((elem) => elem.readNotification == false && elem.company_id == req.session.company_id)
                  
                    res.send(unreadData);
                  }else{
                    let arr = [];
                    res.send(arr)
                  }                  
                    //callback(null, memberData);                        
                }else{
                    //callback(null);
                }
            }
        });
    }                
});




})

app.get('/notifications/read/:user_id/:jobPost_id/:applicant_id',auth.authentic, auth.deleteUser,function(req,res) {
  console.log("from client11...",req.params.user_id);
  console.log(req.params.jobPost_id)
  
  let applicantDetails = req.params.applicant
  console.log("test.......",applicantDetails)
  //let applicant_id = req.params.applicant_id;
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "userdata"; // Define Collection Name


  connectDb(function(err, client){
    if(err){
        throw err;
    }else{
        const db=client.db(databaseName); // Enter Database Name
        db.collection(collectionName).findOneAndUpdate({"user_id":parseInt(req.params.user_id),"applicationNotifications":{
          $elemMatch:{jobPost_id:parseInt(req.params.jobPost_id)}
        }},{
          $set: {
            'applicationNotifications.$[elem].readNotification': true
          }
        },{
           arrayFilters: [ {"elem.applicant_id": parseInt(req.params.applicant_id),"elem.jobPost_id": parseInt(req.params.jobPost_id)} ]
        },

        
        function (err, memberData) {
          
            client.close();
            if(err){
                //callback(err);
            }else{     
                if(memberData){ 

                  
                  res.redirect('/recruiter/profile');
                    //callback(null, memberData);                        
                }else{
                    //callback(null);
                }
            }
        });
    }                
});




})

app.get('/readNotifications',auth.authentic, auth.deleteUser,function(req,res) {
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "userdata"; // Define Collection Name


  connectDb(function(err, client){
    if(err){
        throw err;
    }else{
        const db=client.db(databaseName); // Enter Database Name
        db.collection(collectionName).findOne({"user_id":16}, function (err, memberData) {
            client.close();
            if(err){
                callback(err);
            }else{     
                if(memberData){ 
                  let memberArray = memberData.applicationNotifications
                  let unreadData = memberArray.filter((elem) => elem.applicant_id >= 1)
                  res.send(unreadData);
                    //callback(null, memberData);                        
                }else{
                    //callback(null);
                }
            }
        });
    }                
});




})

// Logout
app.get('/logout', auth.authentic, auth.deleteUser, urlencodedParser, recruiterControllers.logout)


//error page
app.get('/error', function (req, res) {
  res.render('error', {
    errMsg: req.query.msg
  });
})


// forgot Password
app.get('/forgot-password', auth.checkMemberLogin, function (req, res) {
  res.render('pages/recruiter/forgot-password', {
    title: 'Forgot Password',
    sessionData: req.session,
    success: req.session.success,
    errors: req.session.errors,
    layout: 'login',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})


// Fetch Login Details
app.post('/forgot-password', urlencodedParser, recruiterControllers.forgotPassword)

// forgot Password
app.get('/update-password', auth.checkMemberLogin, function (req, res) {
  res.render('pages/recruiter/new-password', {
    title: 'Change Password',
    sessionData: req.session,
    success: req.session.success,
    sendedEmail: req.session.registerdEmail,
    errors: req.session.errors,
    layout: 'login',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})


// Fetch Login Details
app.post('/update-password', urlencodedParser, recruiterControllers.updatePassword)

//Resend OTP
app.get('/update-password/:email',auth.checkMemberLogin,function(req,res) {

  console.log(req.params.email);
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  let collectionName = "recruiterForgotPassword"; // Define Collection Name
  
  
  var rn = require('random-number');
  var options = {
    min:  100000
  , max:  900000
  , integer: true
  }
  var otp = rn(options);

  console.log(otp + "otp created");
  // Check email Unique
  connectDb(function (err, client) {
        if (err) {
          throw err;
        } else {
          const db = client.db(databaseName); // Enter Database Name
          //console.log(req.body.otp);
          //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
          db.collection("recruiterForgotPassword").findOneAndUpdate({
            "email": req.params.email
          },{
            $set:{
              "otp" : otp
            }
          }, function (err, result) {
            console.log(result);
            client.close();
            if (!result) {
              var error = {
                param: "otp",
                msg: "Your OTP does not Match",
                value: req.body.otp
              };
              if (!errors) {
                errors = [];
              }
              errors.push(error);
            } else {
              var nodemailer = require('nodemailer');
              var smtpTransport = require('nodemailer-smtp-transport');

              var mailAccountUser = 'testsmtptt@gmail.com'
              var mailAccountPassword = 'testsmtppassword'
              //var mailAccountPassword =process.env.GMAIL_PASS

              var fromEmailAddress = 'testsmtptt@gmail.com'
              var toEmailAddress = req.params.email

              var transport = nodemailer.createTransport({
                  service: 'gmail',
                  auth: {
                      user: mailAccountUser,
                      pass: mailAccountPassword
                  }
              })

              var mail = {
                  from: fromEmailAddress,
                  to: toEmailAddress,
                  subject: "Change Password",
                  text: "Univerisity",
                  html: "Your OTP  is: <br><p><b>"+otp+"</b></p>"
              }

              transport.sendMail(mail, function(error, response){
                  if(error){
                      console.log(error);
                  }else{
                      console.log("Message sent: ");
                      //console.log(response);
                  }

                  transport.close();
              })
              
              // callback(null, result);
              result = []
              result.push({
                msg: "OTP has send to your email address.."
              });
              req.session.errors = false;
              req.session.success = result;
              console.log("otp in..")
              res.redirect('/recruiter/update-password');
            }
          })
              // Here need  to be next part of action - not outside find callback!
          // cb(errors)
        }
    })

      })


module.exports = app;