const express = require('express');
var bodyParser = require('body-parser');
const fs = require('fs');
var expressValidator = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({
  extended: false
});

const auth = require('../auth/auth'); // Get Auth Function
const configRoute = require('../config/route'); // Get config rotue
const utils = require('../helpers/utility')
var baseUrl = configRoute.baseUrl; // Get Base Url

// Use app
const app = express.Router(); //Get App
app.use(expressValidator()) //Define Express Validator
app.use(bodyParser.json()); // Define Bodyparser 

// Include models and controllers
const Model = require('../model/adminModel'); // include model
const homeControllers = require('../controllers/homes'); // include controller

//Image upload
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/images/uploads')
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + '-' + Date.now())
  }
});
//for multiple file fields
// var upload = multer({
//   storage: storage
// }).array('image',2);
var upload = multer({
   storage: storage
 })


// Image upload
var maxSize = 100 * 1000 * 1000;
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
        let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
      //cb(null, file.fieldname + '-' + Date.now()+ '.pdf')
        cb(null, file.fieldname + '-' + Date.now()+ ext)
    }
});
var uploadPdf = multer({storage: storage, limits: { fileSize: maxSize }});






// Image Upload path based
// let storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//     const dir = __dirname + '/../uploads/member';
//     fs.exists(dir, exist => {
//       if (!exist) {
//         return fs.mkdir(dir, error => cb(error, dir))
//       }
//       return cb(null, dir)
//     })
//   },
//   filename: function (req, file, cb) {
//     let fileOriginalName = utils.slugify(file.originalname);
//     cb(null, (new Date()).getTime() + '-' + fileOriginalName);
//   }
// });

// const fileFilter = (req, file, cb) => {
//   // reject a file
//   if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif') {
//     cb(null, true);
//   } else {
//     cb(null, false);
//   }
// };

// const upload = multer({
//   storage: storage,
//   limits: {
//     fileSize: 1024 * 1024 * 5
//   },
//   fileFilter: fileFilter
// });


// Data Migration 



// Load Registration Page
app.get('/registration', function (req, res) {
  res.render('pages/frontend/registration', {
    title: 'Registration',
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

//load job post page
app.get('/company-details', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  res.render('pages/frontend/basicCompanyDetails', {
    title: 'Registration',
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})
//homeControllers.basicCompanyDetails ,
app.post('/company-details', urlencodedParser, homeControllers.basicCompanyDetails)



// Fetch Registration Details
// app.post('/registration', [urlencodedParser, upload.single('image')], homeControllers.registration)


// Load Registration Page
app.get('/recruter-registration', function (req, res) {
  res.render('pages/frontend/recruterRegistration', {
    title: 'Registration',
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

app.post('/recruter-registration', urlencodedParser, homeControllers.recruterRegistration)



//load Mail registration page
app.get('/new-registration', function (req, res) {
  res.render('pages/frontend/registration/registration', {
    title: 'Mail Registration',
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

//fetch registation
app.post('/new-registration', urlencodedParser, homeControllers.newRegistration);


//email verification
app.get('/new-registration/:token', function (req, res) {
  console.log(req.params.token);
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  let collectionName = "member1"; // Define Collection Name

  // Check email Unique
  connectDb(function (err, client) {
    if (err) {
      throw err;
    } else {
      const db = client.db(databaseName); // Enter Database Name
      //console.log(req.body.otp);
      //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
      db.collection("member1").findOneAndUpdate({
        "token": req.params.token
      }, {
        $set: {
          emailVerified: "true"
        }
      }, function (err, result) {
        client.close();
        if (!result) {
          var error = {
            param: "otp",
            msg: "Your OTP does not Match",
            value: req.body.otp
          };
          if (!errors) {
            errors = [];
          }
          errors.push(error);
        } else {
          result = []
          result.push({
            msg: "Email verified.."
          });
          req.session.errors = false;
          req.session.success = result;
          req.session.emailNotVerified = false;
          console.log("email in..")
          res.redirect('/login');
          //console.log("result ",result)
          //console.log("result email ",result.email)

          //set email verified true in collection

        }
      })
      // Here need  to be next part of action - not outside find callback!
      // cb(errors)
    }
  })

})

// Load Login Page
app.get('/login', auth.checkMemberLogin, function (req, res) {
  res.render('pages/frontend/login', {
    title: 'Login',
    sessionData: req.session,
    success: req.session.success,
    errors: req.session.errors,
    reverify: req.session.emailNotVerified,
    loginEmail: req.session.loginEmail,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})


// Fetch Login Details
app.post('/login', urlencodedParser, auth.emailVerified, homeControllers.login)




//dashboard page
app.get('/dashboard', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  homeControllers.getDashboard(req, res, function (err, appliesCount,saveJobsCount) {
    res.render('pages/frontend/dashboard', {
      title: 'My Dashboard',
      success: req.session.success,
      errors: req.session.errors,
      layout: 'frontend',
      sessionData: req.session,
      appliesCount: appliesCount,
      saveJobsCount: saveJobsCount,
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})

// Load Profile Page
app.get('/profile', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  homeControllers.getProfile(req, res, function (err, result,countries) {
    res.render('pages/frontend/profile', {
      title: 'My Profile',
      success: req.session.success,
      errors: req.session.errors,
      layout: 'frontend',
      sessionData: req.session,
      result: result,
      countries: countries,
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})


//History page
app.get('/history', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  homeControllers.getHistory(req, res, function (err, activeApplications,pastApplications) {
    res.render('pages/frontend/history', {
      title: 'History',
      success: req.session.success,
      errors: req.session.errors,
      layout: 'frontend',
      sessionData: req.session,
      activeApplications: activeApplications,
      pastApplications: pastApplications,
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})

//Wishlist page
app.get('/wishlist', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  homeControllers.getWishlist(req, res, function (err, activeApplications,pastApplications) {
    res.render('pages/frontend/wishlist', {
      title: 'Wishlist',
      success: req.session.success,
      errors: req.session.errors,
      layout: 'frontend',
      sessionData: req.session,
      activeApplications: activeApplications,
      pastApplications: pastApplications,
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})

//Resend reverification email
app.get('/login/:email', auth.checkMemberLogin, function (req, res) {

  console.log(req.params.email);
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "member1"; // Define Collection Name


  let token = cryptoRandomString({
    length: 25,
    type: 'hex'
  })

  let urlToMail = `http://localhost:5000/new-registration/${token}`;



  // Check email Unique
  connectDb(function (err, client) {
    if (err) {
      throw err;
    } else {
      const db = client.db(databaseName); // Enter Database Name
      //console.log(req.body.otp);
      //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
      db.collection("member1").findOneAndUpdate({
        "email": req.params.email
      }, {
        $set: {
          "token": token
        }
      }, function (err, result) {
        console.log(result);
        client.close();
        if (!result) {
          var error = {
            param: "otp",
            msg: "Your OTP does not Match",
            value: req.body.otp
          };
          if (!errors) {
            errors = [];
          }
          errors.push(error);
        } else {
          var nodemailer = require('nodemailer');
          var smtpTransport = require('nodemailer-smtp-transport');

          var mailAccountUser = 'testsmtptt@gmail.com'
          var mailAccountPassword = 'testsmtppassword'
          //var mailAccountPassword =process.env.GMAIL_PASS

          var fromEmailAddress = 'testsmtptt@gmail.com'
          var toEmailAddress = req.params.email

          var transport = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: mailAccountUser,
              pass: mailAccountPassword
            }
          })

          var mail = {
            from: fromEmailAddress,
            to: toEmailAddress,
            subject: "Email Verify",
            text: "Job Portal",
            html: `Please click the link to verify the email:<a href = "${urlToMail}">login link click here...</a>`
          }

          transport.sendMail(mail, function (error, response) {
            if (error) {
              console.log(error);
            } else {
              console.log("Message sent: ");
              //console.log(response);
            }

            transport.close();
          })

          // callback(null, result);
          result = []
          result.push({
            msg: "Your Login link has Send to Your Email address"
          });
          req.session.errors = false;
          req.session.success = result;

          res.redirect('/login');
        }
      })
      // Here need  to be next part of action - not outside find callback!
      // cb(errors)
    }
  })

})




// Fetch Profile Details
app.post('/edit', auth.frontendAuthentic, auth.deleteMember, [urlencodedParser, upload.single('image')], homeControllers.edit)

app.post('/edit_second', auth.frontendAuthentic, auth.deleteMember, [urlencodedParser, uploadPdf.single('image')], homeControllers.edit)


app.post('/edited',auth.frontendAuthentic, auth.deleteUser,function(req,res) {
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "member1"; // Define Collection Name
  let now = new Date();

  console.log("edit1")
   res.send("success..");




})

// Fetch Login Details
app.get('/logout', urlencodedParser, homeControllers.logout)


// Load Home Page
app.get('/', function (req, res) {
  homeControllers.viewCount(req, res, function (err, result) {
    res.render('pages/frontend/home', {
      title: 'Job Poratal',
      active: {
        home: true
      },
      success: req.session.success,
      errors: req.session.errors,
      sessionData: req.session,
      layout: 'frontend',
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})

//show job list page
app.get('/job-list', function (req, res) {
  homeControllers.jobList(req, res, function (err, result,totalData) {
    res.render('pages/frontend/job-lists', {
      title: 'Job Poratal',
      success: req.session.success,
      errors: req.session.errors,
      sessionData: req.session,
      result: result,
      totalData:totalData,
      layout: 'frontend',
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})
//filtered job list
app.get('/job-list/filtered', function (req, res) {
  homeControllers.filteredJobList(req, res, function (err, result,totalData) {
    res.render('pages/frontend/job-lists', {
      title: 'Job Poratal',
      success: req.session.success,
      errors: req.session.errors,
      sessionData: req.session,
      result: result,
      totalData:totalData,
      layout: 'frontend',
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})

//apply job
app.get('/job-list/:application_id/:company_id',auth.frontendAuthentic, auth.deleteMember, function (req, res) {

  const connectDb = require('../config/database');
  const collectionIndex = require('../model/collectionIndex');
  let date = require('date-and-time');
  const configRoute = require('../config/route'); // Get config rotue
  const databaseName=configRoute.databaseName;// Store Database Name
  let now = new Date();

  console.log("............");
  console.log(req.session.member_email)
  console.log(req.params.application_id);
  let applicant_id = req.session.member_id;

  let applicant_email = req.session.member_email;
  var application_id = req.params.application_id;
  
  req.collectionName="memberHistoryCollection";
    collectionIndex.checkIndex(req, res, function(resultId){
  connectDb(function (err, client) {
    if (err) {
      throw err;
    } else {

      const db = client.db(databaseName);
      db.collection("jobPostCollection").findOne({
        "jobPost_id": parseInt(req.params.application_id),
        "applications":{$elemMatch: {"applicant_id":applicant_id}}
      }, function (err,resultValue){
        if(err){
          console.log(err);
          callback(err);
        }else{
          if(resultValue){
            console.log("applied before...")
            res.redirect('/job-list');
          }
          else
          {
            console.log("unique candidate");
            var userApplied = {
              member_application_history_id : resultId,
              member_id : applicant_id,
              jobPost_id : req.params.application_id,
              notSelected : true,
              created_ip_address:req.connection.remoteAddress,
              modified_ip_address:req.connection.remoteAddress,
              created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
              modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
            }
            db.collection("memberHistoryCollection").insertOne(userApplied,function (err,newAppliedJobData) {
              if(err) {
                callback(err)
              }else{
              
            db.collection("jobPostCollection").findOneAndUpdate({
              "jobPost_id": parseInt(req.params.application_id)
              
            }, {
              "$push": {
                "applications": {
                  "applicant_id": applicant_id,
                  "applicant_email": applicant_email,
                  "notSelected":true,
                  
                  "applied_at" : date.format(now, 'YYYY/MM/DD HH:mm:ss')
                }
              }
            }, {
              upsert: true
            },
            function (err, memberData) {
              
              if (err) {
                console.log("same person....")
                callback(err);
              } else {
    
                if (memberData) {
                  
                  console.log(applicant_id);
                  console.log(applicant_email)
                  console.log(date.format(now, 'YYYY/MM/DD HH:mm:ss'));
                  
                  db.collection("userdata").findOneAndUpdate({
                    "user_id": parseInt(memberData.value.user_id)
                    
                  }, {
                    "$push": {
                      "applicationNotifications": {
                        "applicant_id": applicant_id,
                        "user_id":memberData.value.user_id,
                        "jobPost_id" :memberData.value.jobPost_id,
                        "company_id":req.params.company_id,
                        "applicant_email": applicant_email,

                        "message":"New candidate applied",
                        "readNotification":false,
                        "applied_at" : date.format(now, 'YYYY/MM/DD HH:mm:ss')
                      }
                    }
                  }, {
                    upsert: true
                  },
                  function (err, modifiedData) {
                    client.close();
                    if (err) {
                      console.log("same person....")
                      
                    } else {
          
                      if (modifiedData) {
                        
      
                        res.redirect('/job-list');
                      } else {
                        console.log("error")
                      }
          
          
                    }
                  });

                  // res.redirect('/job-list');
                } else {
                  console.log("error")
                }
    
    
              }
            });
              
            }
          })


          }
        }
      })
      
    }
  });
  });
})
//save job
app.get('/job-list/save-job/:application_id',auth.frontendAuthentic, auth.deleteMember, function (req, res) {

  const connectDb = require('../config/database');
  const collectionIndex = require('../model/collectionIndex');
  let date = require('date-and-time');
  const configRoute = require('../config/route'); // Get config rotue
  const databaseName=configRoute.databaseName;// Store Database Name
  let now = new Date();

  console.log("........sr");
  console.log(req.session.member_email)
  console.log(req.params.application_id);
  let applicant_id = req.session.member_id;

  let applicant_email = req.session.member_email;
  var application_id = req.params.application_id;
  
  req.collectionName="memberSaveJobCollection";
    collectionIndex.checkIndex(req, res, function(resultId){
  connectDb(function (err, client) {
    if (err) {
      throw err;
    } else {

      const db = client.db(databaseName);
      db.collection("jobPostCollection").findOne({
        "jobPost_id": parseInt(req.params.application_id),
        "saveJobs":{$elemMatch: {"applicant_id":applicant_id}}
      }, function (err,resultValue){
        if(err){
          console.log(err);
          callback(err);
        }else{
          if(resultValue){
            console.log("applied before...")
            res.redirect('/job-list');
          }
          else
          {
            console.log("unique candidate");
            var userApplied = {
              member_application_history_id : resultId,
              member_id : applicant_id,
              jobPost_id : req.params.application_id,
              notSelected : true,
              created_ip_address:req.connection.remoteAddress,
              modified_ip_address:req.connection.remoteAddress,
              created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
              modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
            }
            db.collection("memberSaveJobCollection").insertOne(userApplied,function (err,newAppliedJobData) {
              if(err) {
                callback(err)
              }else{
              
            db.collection("jobPostCollection").findOneAndUpdate({
              "jobPost_id": parseInt(req.params.application_id)
              
            }, {
              "$push": {
                "saveJobs": {
                  "applicant_id": applicant_id,
                  "applicant_email": applicant_email,
                  "notSelected":true,
                  
                  "applied_at" : date.format(now, 'YYYY/MM/DD HH:mm:ss')
                }
              }
            }, {
              upsert: true
            },
            function (err, memberData) {
              
              if (err) {
                console.log("same person....")
                callback(err);
              } else {
    
                if (memberData) {
                  
                  res.redirect('/job-list');

                  // res.redirect('/job-list');
                } else {
                  console.log("error")
                }
    
    
              }
            });
              
            }
          })


          }
        }
      })
      
    }
  });
  });
})


//job searched post
app.post('/job-list/searched',urlencodedParser, function (req, res) {
  
  homeControllers.searchedJobs(req, res, function (err, result,totalData) {
    res.render('pages/frontend/searched-job-lists', {
      title: 'Job Poratal',
      success: req.session.success,
      errors: req.session.errors,
      
      sessionData: req.session,
      result: result,
      totalData:totalData,
      layout: 'frontend',
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})

app.post('/job-list/filter',urlencodedParser, homeControllers.filteredJobs
)

//job searched get
app.get('/searched-job-list',function (req, res) {
  homeControllers.searchedJobsGet(req, res, function (err, result) {
    res.render('pages/frontend/searched-job-lists', {
      title: 'Job Poratal',
      success: req.session.success,
      errors: req.session.errors,
      sessionData: req.session,
      result: result,
      layout: 'frontend',
      baseUrl: baseUrl
    });
    req.session.errors = null;
    req.session.success = null;
  });
})

// Load Executive Council Page
app.get('/executiveCouncil', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  res.render('pages/frontend/executiveCouncil', {
    title: 'Executive Council',
    active: {
      spatialScientists: true
    },
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})


//notifications panel
app.get('/notifications/:member_id',function(req,res) {
  console.log("id......"+req.params.member_id);
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "memberNotification"; // Define Collection Name


  connectDb(function(err, client){
    if(err){
        throw err;
    }else{
        const db=client.db(databaseName); // Enter Database Name
        db.collection("memberNotification").find({"member_id":req.params.member_id,notificationNotReaded:true}).toArray(function(err, memberData) {
          console.log("1111....",memberData)
          
          if(err){
              callback(err);
          }else{     
              if(memberData){ 
              
                
                res.send(memberData);
                  //callback(null, memberData);                        
              }else{
                  //callback(null);
              }
          }
      }) ;
    }                
});




})

// //notification read panel
// app.get('/notifications/read/:user_id',auth.frontendAuthentic, auth.deleteMember,function(req,res) {
//   console.log("from client11...",req.params.user_id);
//   console.log(req.params.jobPost_id)
  
//   let applicantDetails = req.params.applicant
//   console.log("test.......",applicantDetails)
//   //let applicant_id = req.params.applicant_id;
//   const configRoute = require('../config/route'); // Get config rotue
//   const connectDb = require('../config/database');
//   let databaseName = configRoute.databaseName; // Store Database Name
//   const cryptoRandomString = require('crypto-random-string');
//   let collectionName = "userdata"; // Define Collection Name


//   connectDb(function(err, client){
//     if(err){
//         throw err;
//     }else{
//         const db=client.db(databaseName); // Enter Database Name
//         db.collection(collectionName).findOneAndUpdate({"user_id":parseInt(req.params.user_id),"applicationNotifications":{
//           $elemMatch:{jobPost_id:parseInt(req.params.jobPost_id)}
//         }},{
//           $set: {
//             'applicationNotifications.$[elem].readNotification': true
//           }
//         },{
//            arrayFilters: [ {"elem.applicant_id": parseInt(req.params.applicant_id),"elem.jobPost_id": parseInt(req.params.jobPost_id)} ]
//         },

        
//         function (err, memberData) {
          
//             client.close();
//             if(err){
//                 //callback(err);
//             }else{     
//                 if(memberData){ 

                  
//                   res.redirect('/profile');
//                     //callback(null, memberData);                        
//                 }else{
//                     //callback(null);
//                 }
//             }
//         });
//     }                
// });




// })

app.get('/notifications/read/:memberNotification_id/:member_id',auth.frontendAuthentic, auth.deleteMember,function(req,res) {
  console.log("from client11...",req.params.memberNotification_id);

  //let applicant_id = req.params.applicant_id;
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  const cryptoRandomString = require('crypto-random-string');
  let collectionName = "memberNotification"; // Define Collection Name


  connectDb(function(err, client){
    if(err){
        throw err;
    }else{
        const db=client.db(databaseName); // Enter Database Name
        db.collection(collectionName).findOneAndUpdate({"memberNotification_id":parseInt(req.params.memberNotification_id)},{
          $set: {
            "notificationNotReaded": false
          }
        },
        function (err, memberData) {
          db.collection("memberNotification").find({"member_id":req.params.member_id,notificationNotReaded:true}).toArray(function(err, memberDataModified) {
            
            
            if(err){
              throw err;
            }else{     
                if(memberDataModified){ 
                
                  console.lo
                  res.send(memberDataModified);
                    //callback(null, memberData);                        
                }else{
                    //callback(null);
                }
            }
        }) ;

        });
    }                
});




})






// Load Working Committee Page


// Load Editorial Board Page


// Load Abstracting / Indexing  Page


// Load Subcription  Page
app.get('/subcription', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  res.render('pages/frontend/subcription', {
    title: 'Subcription',
    active: {
      generalInformation: true
    },
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

// Load Subcription  Page
app.get('/contact', function (req, res) {
  res.render('pages/frontend/contact', {
    title: 'Contact Us',
    active: {
      contact: true
    },
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

// Load Publication Page
app.get('/ethics', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  res.render('pages/frontend/publicationEthics', {
    title: 'Publication Ethics',
    active: {
      generalInformation: true
    },
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

// Load Notice Page
app.get('/notice', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  homeControllers.getNotices(req, res, function (err, result) {
    //console.log(result);
    homeControllers.getlatestNotice(req, res, function (err, data) {
      //console.log(data);
      res.render('pages/frontend/notice', {
        title: 'Notice',
        active: {
          generalInformation: true
        },
        success: req.session.success,
        errors: req.session.errors,
        sessionData: req.session,
        layout: 'frontend',
        baseUrl: baseUrl,
        result: result,
        data: data
      });
      req.session.errors = null;
      req.session.success = null;
    });
    // res.render('pages/frontend/notice',{title: 'Notice', active: {generalInformation: true }, success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'frontend', baseUrl:baseUrl, result:result});
    // req.session.errors= null; req.session.success=null;
  });
})

app.get('/singleNotice', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  homeControllers.getNotice(req, res, function (err, result) {
    res.render('pages/frontend/singleNotice', {
      title: 'Notice',
      active: {
        generalInformation: true
      },
      success: req.session.success,
      errors: req.session.errors,
      sessionData: req.session,
      layout: 'frontend',
      baseUrl: baseUrl,
      result: result
    });
    req.session.errors = null;
    req.session.success = null;
  });
})


// Load Origin and Aim Page
app.get('/originAim', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  res.render('pages/frontend/originAim', {
    title: 'Origin and Aim',
    active: {
      spatialScientists: true
    },
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

// Load Constitution Page


//Payment Page
app.get('/payment', auth.frontendAuthentic, auth.deleteMember, function (req, res) {
  res.render('pages/frontend/payment', {
    title: 'Registration',
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})


// Load Profile Page
var request = require('request');
const Razorpay = require('razorpay');
const instance = new Razorpay({
  key_id: 'rzp_test_MUXBBpeX9feKJw',
  key_secret: '1VNmVmVHsmvrseTdylukA46H',
});

app.get('/purchase/:razorpay_payment_id', auth.frontendAuthentic, auth.deleteMember, urlencodedParser, function (req, res) {

  var razorpay_payment_id = req.params.razorpay_payment_id;
  request('https://rzp_test_MUXBBpeX9feKJw:1VNmVmVHsmvrseTdylukA46H@api.razorpay.com/v1/payments/' + razorpay_payment_id, function (error, response, body) {
    //console.log('Response:', JSON.stringify(body));
    homeControllers.payment(req, res, body);
  });

})


// forgot Password
app.get('/forgotPassword',function (req, res) {
  res.render('pages/frontend/forgotPassword', {
    title: 'Forgot Password',
    sessionData: req.session,
    success: req.session.success,
    errors: req.session.errors,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})


// Fetch Login Details
app.post('/forgotPassword', urlencodedParser, homeControllers.forgotPassword)


// forgot Password
app.get('/updatePassword', function (req, res) {
  res.render('pages/frontend/newPassword', {
    title: 'Change Password',
    sessionData: req.session,
    success: req.session.success,
    sendedEmail: req.session.registerdEmail,
    errors: req.session.errors,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})


// Fetch Login Details
app.post('/updatePassword', urlencodedParser, homeControllers.updatePassword)



//Resend OTP
app.get('/updatePassword/:email', auth.checkMemberLogin, function (req, res) {

  console.log(req.params.email);
  const configRoute = require('../config/route'); // Get config rotue
  const connectDb = require('../config/database');
  let databaseName = configRoute.databaseName; // Store Database Name
  let collectionName = "forgotPassword"; // Define Collection Name


  var rn = require('random-number');
  var options = {
    min: 100000,
    max: 900000,
    integer: true
  }
  var otp = rn(options);

  console.log(otp + "otp created");
  // Check email Unique
  connectDb(function (err, client) {
    if (err) {
      throw err;
    } else {
      const db = client.db(databaseName); // Enter Database Name
      //console.log(req.body.otp);
      //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
      db.collection("forgotPassword").findOneAndUpdate({
        "email": req.params.email
      }, {
        $set: {
          "otp": otp
        }
      }, function (err, result) {
        console.log(result);
        client.close();
        if (!result) {
          var error = {
            param: "otp",
            msg: "Your OTP does not Match",
            value: req.body.otp
          };
          if (!errors) {
            errors = [];
          }
          errors.push(error);
        } else {
          var nodemailer = require('nodemailer');
          var smtpTransport = require('nodemailer-smtp-transport');

          var mailAccountUser = 'testsmtptt@gmail.com'
          var mailAccountPassword = 'testsmtppassword'
          //var mailAccountPassword =process.env.GMAIL_PASS

          var fromEmailAddress = 'testsmtptt@gmail.com'
          var toEmailAddress = req.params.email

          var transport = nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: mailAccountUser,
              pass: mailAccountPassword
            }
          })

          var mail = {
            from: fromEmailAddress,
            to: toEmailAddress,
            subject: "Change Password",
            text: "Univerisity",
            html: "Your OTP  is: <br><p><b>" + otp + "</b></p>"
          }

          transport.sendMail(mail, function (error, response) {
            if (error) {
              console.log(error);
            } else {
              console.log("Message sent: ");
              //console.log(response);
            }

            transport.close();
          })

          // callback(null, result);
          result = []
          result.push({
            msg: "OTP has send to your email address.."
          });
          req.session.errors = false;
          req.session.success = result;
          console.log("otp in..")
          res.redirect('/updatePassword');
        }
      })
      // Here need  to be next part of action - not outside find callback!
      // cb(errors)
    }
  })

})


// Load cv Page
app.get('/cv', function (req, res) {
  res.render('pages/frontend/cv', {
    title: 'CURRICULUM VITAE',
    active: {
      home: true
    },
    success: req.session.success,
    errors: req.session.errors,
    sessionData: req.session,
    layout: 'frontend',
    baseUrl: baseUrl
  });
  req.session.errors = null;
  req.session.success = null;
})

app.get('/country-state',function (req,res){
  console.log(req.query.id);
  
  const express=require('express');
  const expressValidator  = require('express-validator');
  const connectDb= require('../config/database');
  const configRoute=require('../config/route'); // Get config rotue
  

  
  //const documentImage = require('../public/images/uploads/noImage.png');
  
  const databaseName=configRoute.databaseName;// Store Database Name
  const collectionName="states"; // Define Collection Name
  
  
  //Get App
  const app= express.Router();
  app.use(expressValidator())
  
  

  connectDb(function(err, client){
    if(err){
        throw err;
    }else{
        const db=client.db(databaseName); // Enter Database Name
        db.collection(collectionName).find({"country_id":req.query.id}).toArray(function (err, noticeData) {
            client.close();
            if(err){
                callback(err);
            }else{                   
                //console.log(noticeData);
                //return;
                if(noticeData){ 
                    res.send(noticeData);
                    //callback(null, noticeData);                        
                }else{
                    callback(null);
                }
            }
        });
    }                
});


})



module.exports = app;