const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

const auth=require('../auth/auth'); // Get Auth Function
const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url

// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser 

// Include models and controllers
const Model = require('../model/recruiterModel');     // include model
const companyControllers = require('../controllers/companies');  // include controller

// Image upload
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});


//all job company routes

    //add company get
  app.get('/add',auth.authentic, auth.deleteUser, function(req, res){
    res.render('pages/company/add-company',{title: 'Add-Company', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', baseUrl:baseUrl});
    req.session.errors= null; req.session.success=null;
  })
  //homeControllers.basicCompanyDetails ,
  app.post('/add',auth.authentic, auth.deleteUser,[urlencodedParser, upload.single('certificates')],companyControllers.addNewCompany )


  //rejected company list
  app.get('/rejected-company-list',auth.authentic, auth.deleteUser, function(req, res){
    companyControllers.rejectedList(req, res, function(err, result){
      res.render('pages/company/rejected-company-list',{title: 'rejected-company-list', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })


  module.exports=app;
  
 