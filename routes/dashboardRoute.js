const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var mailer = require('nodemailer');

const auth=require('../auth/auth'); // Get Auth Function
const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url

var async = require('async');

// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser   
// app.use(expressSession({        // Define Express Session
//     secret: 'secretkey',
//     saveUninitialized: true,
//     resave: true
// }));

// Include models and controllers     // include model
const dashboardsControllers = require('../controllers/dashboards');  // include controller

// Image upload
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});


// Manuscript Rotues


// Load Dashboard
app.get('/', auth.authentic, auth.deleteUser, function(req, res){  
  var member_path='public/documents/members.csv';
  var author_path='public/documents/authors.csv';
  var article_path='public/documents/articles.csv';
  async.parallel({
      dashboard: function(parallelCb) {
          dashboardsControllers.getDashboard(req, res, function(err, result){
          data=result;
          parallelCb(null, result);
          //console.log(result);
        });
      },
      members: function(parallelCb) {
        dashboardsControllers.getMembers(req, res, function(err, result){
          //data=result.length;

          const createCsvWriter = require('csv-writer').createObjectCsvWriter;
          const csvWriter = createCsvWriter({
              path: member_path,
              header: [
                  {id: 'name', title: 'Member Name'}
              ]
          });
          const records = result;
          
          csvWriter.writeRecords(records)       // returns a promise
              .then(() => {
                  //console.log('...Done');
              });

          //console.log("done");

          parallelCb(null, result);
        });
      },
      journal: function(parallelCb) {
        dashboardsControllers.getJournals(req, res, function(err, result){
          //data=result.length;
          const createCsvWriter = require('csv-writer').createObjectCsvWriter;
          const csvWriter = createCsvWriter({
              path: author_path,
              header: [
                  {id: 'author', title: 'Author Name'}
              ]
          });
          const records = result;
          
          csvWriter.writeRecords(records)       // returns a promise
              .then(() => {
                  //console.log('...Done');
              });

          //console.log("done");

          // create article csv
          const csvWriter_article = createCsvWriter({
            path: article_path,
            header: [
                {id: 'title', title: 'Article Titile'}
            ]
          });
          const records_article = result;
          
          csvWriter_article.writeRecords(records_article)       // returns a promise
              .then(() => {
                  //console.log('...Done');
              });

          //console.log("done");

          parallelCb(null, result);
        });
      }

      
    },
    function(err, results) {

      dashboard=results.dashboard;
      members=results.members;
      journals=results.journal;  

    res.render('pages/dashboard/listing',{title: 'Dashboard', active: {dashboard: true }, success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', dashboard:dashboard, journals:journals, members:members, member_path:member_path, author_path:author_path, article_path:article_path,  baseUrl:baseUrl});
        req.session.errors= null; req.session.success=null;
  });
    
})


//Member Listing

// Load Member listing Page
app.get('/articleListing', auth.authentic, auth.deleteUser, function(req, res){  
  dashboardsControllers.getJournals(req, res, function(err, result){
    res.render('pages/dashboard/article_listing',{title: 'Profile', success:req.session.success, errors:req.session.errors, layout: 'admin', sessionData:req.session, result:result, baseUrl:baseUrl});
    req.session.errors= null; req.session.success=null;
  });    
})

// Load Member listing Page
app.get('/articleListing', auth.authentic, auth.deleteUser, function(req, res){  
  dashboardsControllers.getJournals(req, res, function(err, result){
    res.render('pages/dashboard/article_listing',{title: 'Profile', success:req.session.success, errors:req.session.errors, layout: 'admin', sessionData:req.session, result:result, baseUrl:baseUrl});
    req.session.errors= null; req.session.success=null;
  });    
})




// Send mail


app.get('/mail', auth.authentic, auth.deleteUser, function(req, res){

  var nodemailer = require('nodemailer');
  var smtpTransport = require('nodemailer-smtp-transport');

  var mailAccountUser = 'raja.dey1908@gmail.com'
  var mailAccountPassword = 'ijulbssgtafqqbqn'
  //var mailAccountPassword =process.env.GMAIL_PASS

  var fromEmailAddress = 'raja.dey1908@gmail.com'
  var toEmailAddress = 'raja.btkol@gmail.com'

  var transport = nodemailer.createTransport({
      service: 'gmail',
      auth: {
          user: mailAccountUser,
          pass: mailAccountPassword
      }
  })

  var mail = {
      from: fromEmailAddress,
      to: toEmailAddress,
      subject: "hello world!",
      text: "Hello!",
      html: "<b>Hello!</b><p><a href=\"http://www.yahoo.com\">Click Here</a></p>"
  }

  transport.sendMail(mail, function(error, response){
      if(error){
          console.log(error);
      }else{
          console.log("Message sent: " + response.message);
          console.log(response);
      }

      transport.close();
  });

})



module.exports=app;


    