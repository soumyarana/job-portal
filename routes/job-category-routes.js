const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
//var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

const auth=require('../auth/auth'); // Get Auth Function
const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url

// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser 

// Include models and controllers
const Model = require('../model/recruiterModel');     // include model
const jobCategoryControllers = require('../controllers/job-category');  // include controller

// Image upload
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, 'public/images/uploads')       
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});


//all job category routes

    //get category
  app.get('/add',auth.authentic, auth.deleteUser, function(req, res){
    res.render('pages/job-category/add-category',{title: 'Add-Category', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', baseUrl:baseUrl});
    req.session.errors= null; req.session.success=null;
  })
  //homeControllers.basicCompanyDetails ,
  app.post('/add',auth.authentic, auth.deleteUser,urlencodedParser,jobCategoryControllers.addNewCategory )

  //get category list
  app.get('/job-category-list',auth.authentic, auth.deleteUser, function(req, res){
    jobCategoryControllers.getJobCategory(req, res, function(err, result){
      res.render('pages/job-category/category-list',{title: 'Job Category List', success:req.session.success, errors:req.session.errors, sessionData:req.session,  layout: 'admin', result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;

    })
    
  })

  // Load Edit Page
  app.get('/edit',auth.authentic, auth.deleteUser, function(req, res){
    jobCategoryControllers.editGetCategory(req, res, function(err, result){
      res.render('pages/job-category/edit-category',{title: 'Edit', success:req.session.success, errors:req.session.errors, layout: 'admin', sessionData:req.session, result:result, baseUrl:baseUrl});
      req.session.errors= null; req.session.success=null;
    });
  })
  // Fetch Edit Details
  app.post('/edit',auth.authentic, auth.deleteUser, urlencodedParser, jobCategoryControllers.editCategory)

  app.get('/delete',auth.authentic, auth.deleteUser, urlencodedParser, jobCategoryControllers.deleteCategory)

  module.exports=app;
  
 