var MongoClient=require('mongodb').MongoClient;
var url='mongodb://localhost:27017/university';

//var url='mongodb://universityUser:nopass123@139.180.213.112:27017/university';

//var url='mongodb://localhost:27017';

module.exports= function(callback){
    MongoClient.connect(url,{useNewUrlParser: true}, function(err, db){
        if(err){
            throw err;
        }else{
            console.log("Database Connected");
        }
        db.on('close', () => { console.log('MongoDB-> Closed connection'); });
        callback(err, db);
    })
}