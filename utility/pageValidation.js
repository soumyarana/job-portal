const configRoute=require('../config/route'); // Get config rotue
const connectDb= require('../config/database');
var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="userdata"; // Define Collection Name

// Add Validation
module.exports.add= function(req, res ,cb){
    req.check('title', 'Please Enter Title').notEmpty();
    req.check('slug', 'Please Enter Slug').notEmpty();
    req.check('description', 'Please Enter Description').notEmpty();  
    // Store errors
    var errors=req.validationErrors();
    cb(errors);    
        
}

// Edit Validation
module.exports.edit= function(req, res ,cb){
    req.check('title', 'Please Enter Title').notEmpty();
    req.check('slug', 'Please Enter Author').notEmpty();
    req.check('description', 'Please Enter Description').notEmpty();  
    // Store errors
    var errors=req.validationErrors();
    cb(errors);    
        
}

