const configRoute=require('../config/route'); // Get config rotue
const connectDb= require('../config/database');
var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="member1"; // Define Collection Name

// Add Validation
module.exports.basicCompanyDetails = function(req, res ,cb){
    req.check('companyName', 'Please Enter Author').isLength({ min: 2 });
    // req.check('title', 'Please Enter Title').notEmpty();
    // req.check('location', 'Please Enter Description').notEmpty();  
    // Store errors
    var errors=req.validationErrors();
    cb(errors);    
        
}

// Edit Validation
module.exports.edit= function(req, res ,cb){
    req.check('title', 'Please Enter Title').notEmpty();
    req.check('author', 'Please Enter Author').notEmpty();
    req.check('description', 'Please Enter Description').notEmpty();  
    // Store errors
    var errors=req.validationErrors();
    cb(errors);    
        
}

