const configRoute=require('../config/route'); // Get config rotue
const connectDb= require('../config/database');
var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="userdata"; // Define Collection Name

// Registration Validation
module.exports.registration= function(req, res ,cb){
     

    // Store errors
    var errors=req.validationErrors();

    // Check email Unique
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name

            db.collection(collectionName).findOne({ email: req.body.email }, function(err,result){
                client.close();
                if(result){
                    var error = {param: "email", msg: "Email address already registered", value: req.body.email};
                    if (!errors) {
                        errors = [];
                    }
                    errors.push(error); 
                    
                }
                cb(errors)
                // Here need  to be next part of action - not outside find callback!
            });
        }                    
    })
        
}

// Profile Validation
module.exports.profile= function(req, res ,cb){
    req.check('first_name', 'Please Enter First Name').notEmpty();
    req.check('last_name', 'Please Enter Last Name').notEmpty();
    //req.check('email', 'Invalid Email').isEmail();
    req.check('phone', 'Phone Number Must be 8 to 10 Digit').isLength({min:8},{max:10});
    req.check('username', 'Please Enter Username').notEmpty(); 

    // Store errors
    var errors=req.validationErrors();
    //Check email Unique
    if(req.body.email==req.body.old_email){
        cb(errors)
    }else{
        connectDb(function(err, client){
            if(err){
                throw err;
            }else{
                const db=client.db(databaseName); // Enter Database Name
    
                db.collection(collectionName).findOne({ email: req.body.email }, function(err,result){
                    client.close();
                    if(result){
                        var error = {param: "email", msg: "Email address already registered", value: req.body.email};
                        if (!errors) {
                            errors = [];
                        }
                        errors.push(error); 
                        
                    }
                    console.log(errors);
                    cb(errors)
                    // Here need  to be next part of action - not outside find callback!
                });
            }                    
        })
    }
    
    //cb(errors)
}

// Login
module.exports.login= function(req, res ,cb){
    
    var errors=req.validationErrors();
    cb(errors)
}

// Forget password 
module.exports.forgotPassword= function(req, res ,cb){
    //req.check('email', 'Invalid Email address').isEmail();
    //req.check('password', 'Password Need Minimum 6 character ').isLength({ min: 6 });

    // Store errors
    var errors=req.validationErrors();

    // Check email Unique
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name

            db.collection(collectionName).findOne({ email: req.body.email }, function(err,result){
                client.close();
                if(!result){
                    var error = {param: "email", msg: "Email address does not exist", value: req.body.email};
                    if (!errors) {
                        errors = [];
                    }
                    errors.push(error);                     
                }
                cb(errors)
                // Here need  to be next part of action - not outside find callback!
            });
        }                    
    })
    // var errors=req.validationErrors();
    // cb(errors)
}


// Update password 
module.exports.updatePassword= function(req, res ,cb){
    // req.check('password', 'Password Need Minimum 6 character ').isLength({ min: 6 });
    // req.checkBody('confirm_password', 'Confirm Password do not match').equals(req.body.password);

    // Store errors
    var errors=req.validationErrors();

    // Check email Unique
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            //console.log(req.body.otp);
            //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
                db.collection("recruiterForgotPassword").findOne({ "otp":parseInt(req.body.otp)}, function(err,result){
                client.close();                
                if(!result){
                    var error = {param: "otp", msg: "Your OTP does not Match", value: req.body.otp};
                    if (!errors) {
                        errors = [];
                    }
                    errors.push(error);                     
                }else{
                    req.body.email=result.email;
                }
                cb(errors)
                // Here need  to be next part of action - not outside find callback!
            });
        }                    
    })
    // var errors=req.validationErrors();
    // cb(errors)
}
