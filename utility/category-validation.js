
const configRoute=require('../config/route'); // Get config rotue
const connectDb= require('../config/database');
var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="jobCategoryCollection"; // Define Collection Name

// Add Validation
module.exports.addNewCategory = function(req, res ,cb){
    req.check('categoryName', 'Please Enter Category Name').notEmpty();
    req.check('categoryDetails', 'Please Enter Category Details').notEmpty();
    // req.check('location', 'Please Enter Description').notEmpty();  
    // Store errors
    var errors=req.validationErrors();
     
    
    // check category unique
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name

            db.collection(collectionName).findOne({user_id:req.session.user_id, categoryName: req.body.categoryName}, function(err,result){
                client.close();
                if(result){
                    var error = {param: "email", msg: "Same category already exists", value: req.body.email};
                    if (!errors) {
                        errors = [];
                    }
                    errors.push(error); 
                    
                }
                cb(errors)
                // Here need  to be next part of action - not outside find callback!
            });
        }                    
    })
        
}

//edit validation
module.exports.editCategory = function(req, res ,cb){
    req.check('categoryName', 'Please Enter Category Name').notEmpty();
    req.check('categoryDetails', 'Please Enter Category Details').notEmpty();
    // req.check('location', 'Please Enter Description').notEmpty();  
    // Store errors
    var errors=req.validationErrors();
    cb(errors);    
        
}
