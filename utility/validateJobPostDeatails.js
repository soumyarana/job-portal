const configRoute=require('../config/route'); // Get config rotue
const connectDb= require('../config/database');
var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="jobPostCollection"; // Define Collection Name

// Add Validation
module.exports.addNewJob = function(req, res ,cb){
    req.check('companyName', 'Please Enter Company Name').notEmpty();
    req.check('title', 'Please Enter Title').notEmpty();
    req.check('location', 'Please Enter Description').notEmpty();
    req.check('jobCategory', 'Please Select a Categopry').notEmpty();
    req.check('jobType', 'Please Select Job Type').notEmpty();
    req.check('description', 'Please Enter Description').notEmpty();

    // Store errors
    var errors=req.validationErrors();
    cb(errors);    
        
}
