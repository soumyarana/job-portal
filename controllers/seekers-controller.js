const validate = require('../utility/homeValidation.js');
const validateCompanyDetails = require('../utility/companyDetailsValidation.js');
const validateCategory = require('../utility/category-validation');
const seekersModel = require('../model/seekers-model');

var https = require('https');




//job application list get
module.exports.getAllApplications = function(req, res,callback){

    seekersModel.getAllApplications(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result);       
        }
    }) 

} 