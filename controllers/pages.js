const validate=require('../utility/pageValidation.js');
const pageModel=require('../model/pageModel.js');


// Add Journal
module.exports.add= function(req, res){
    //console.log("enter")
    console.log(req.body)
    validate.add(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/admin/page/add');
        }else{
            req.session.success=true; 
            pageModel.add(req, res, function(err,result){
                //return
                if(err){
                    throw err;                    
                }{
                    res.redirect('/admin/page/listing');                    
                }
            })     
        }
        
    });    
}

// For Journals
module.exports.getPages= function(req, res, callback){ 
    pageModel.getPages(req, res, function(err,result){
        if(err){
            callback(err);
        }{
            //console.log(result);
            callback(null, result);       
        }
    })                
}

// For Journal
module.exports.getPage= function(req, res, callback){ 
    pageModel.getPage(req, res, function(err,result){
        if(err){
            callback(err);
        }{
            //console.log(result);
            callback(null, result);       
        }
    })                
}

// Delete Journal
module.exports.delete= function(req, res){
    //console.log(req.query.user_id);
    pageModel.deleteJournal(req, res, function(err,result){
        if(err){
            throw err;
        }{
            res.redirect('/admin/page/listing');      
        }
    }) 
}


module.exports.edit= function(req, res){
    validate.edit(req, res, function(errors){
        if(errors){
            //console.log(req.body.page_id);
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/admin/page/edit?page_id='+req.body.page_id);
        }else{
            req.session.success=true;  
            pageModel.edit(req, res, function(err,result){
                if(err){
                    throw(err);
                }else{
                    res.redirect('/admin/page/listing');                                       
                }
            })     
        }        
    });    
}












module.exports.profile= function(req, res){
    validate.profile(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/admin_routes/profile');
        }else{
            req.session.success=true;  
            pageModel.profile(req, res, function(err,result){
                if(err){
                    throw(err);
                }else{
                    if(req.body.email==req.body.old_email){
                        if(result){
                            res.redirect('/admin_routes/profile');
                        }else{
                            res.redirect('/admin_routes/logout');
                        } 
                    }else{
                        res.redirect('/admin_routes/logout');
                    }
                                       
                }
            })     
        }        
    });    
}



module.exports.login= function(req, res){    
    validate.login(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/admin_routes/login');
        }else{
            req.session.success=true;
            pageModel.login(req,res,function(err,result){
                if(err){
                    throw err;
                }else{
                    if(result==true){
                        req.session.success=false;
                        res.redirect('/admin_routes/profile'); 
                    }else{
                        req.session.errors=[result];
                        req.session.success=false;
                        res.redirect('/admin_routes/login');
                    }
                }                
            })            
        }
    });    
}

module.exports.logout= function(req, res){
    req.session.destroy(function(err) {
        if(err) {
            throw(err);
        } else {
            res.redirect('/admin_routes/login');
        }
    });
}


module.exports.checkValidUser= function(req, res, callback){
    pageModel.getProfile(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            if(result){              
                callback(null, result);
            }else{
                callback(null);
            }             
        }
    })
}


// Status
module.exports.status= function(req, res){
    pageModel.status(req, res, function(err,result){
        if(err){
            throw err;
        }{
            res.redirect('/admin/page/listing');      
        }
    }) 
}