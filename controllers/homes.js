const validate = require('../utility/homeValidation.js');
const validateCompanyDetails = require('../utility/companyDetailsValidation.js');
const homeModel = require('../model/homeModel.js');

const express=require('express');
const app= express();
let http = require("http").createServer(app);
let io = require("socket.io")(http)

var https = require('https');
// Add Member
module.exports.registration = function (req, res) {
    console.log(req.body);
    validate.registration(req, res, function (errors) {
        if (errors) {
            console.log(errors);
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/registration');
        } else {

            // console.log(req);
            // verifyRecaptcha(req.body["g-recaptcha-response"], function (success) {
            //     if (success) {
                    // res.end("Success!");
                    // // TODO: do registration using params in req.body

                    homeModel.registration(req, res, function (err, result) {
                        if (err) {
                            throw err;
                        } else {
                            if (result) {
                                // console.log(result);
                                //    recaptcha 
                                result = []
                                result.push({
                                    msg: "You Have Successfully Register "
                                });
                                req.session.errors = false;
                                req.session.success = result;
                                res.redirect('/login');
                            } else {
                                req.session.errors = errors;
                                req.session.success = false;
                                res.redirect('/registration');
                            }

                        }
                    })
                // } else {
                //     req.session.errors = [{
                //         location: 'body',
                //         param: 'gcaptcha',
                //         msg: 'Captcha verification failed',
                //         value: ''
                //     }];
                //     req.session.success = false;
                //     res.redirect('/registration');
                // }




            // });
        }
    });
};


// Login Member
module.exports.login = function (req, res) {
    validate.login(req, res, function (errors) {
        if (errors) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/login');
        } else {


            // verifyRecaptcha(req.body["g-recaptcha-response"], function (success) {
            //     if (success) {


                    homeModel.login(req, res, function (err, result) {
                        if (err) {
                            throw err;
                        } else {
                            if (result == true) {
                                //console.log(result);
                                // result={msg: "Success "};
                                result = []
                                result.push({
                                    msg: "You Have Successfully Login "
                                });
                                req.session.errors = false;
                                req.session.success = result;
                                req.session.success=true;
                                console.log(req.session.success);
                                {msg: "Success "}
                                console.log("You Have Successfully Login")
                                res.redirect('/profile');
                            } else {
                                req.session.errors = [result];
                                req.session.success = false;
                                //console.log(req.session.errors);
                                res.redirect('/login');
                            }
                        }
                    })
                // } else {
                //     req.session.errors = [{
                //         location: 'body',
                //         param: 'gcaptcha',
                //         msg: 'Captcha verification failed',
                //         value: ''
                //     }];
                //     req.session.success = false;
                //     res.redirect('/login');

                // }
            // });
        }
    });
};
// For profile
module.exports.getProfile = function (req, res, callback) {
    
    homeModel.getProfile(req, res, function (err, result,countries) {
        if (err) {
            callback(err);
        } else {
            
            callback(null, result,countries);
        }
    })
}

//Dashboard controller
module.exports.getDashboard = function (req, res, callback) {
    
    homeModel.getDashboard(req, res, function (err, appliesCount,saveJobsCount) {
        if (err) {
            callback(err);
        } else {
            
            callback(null, appliesCount,saveJobsCount);
        }
    })
}

//History controller
module.exports.getHistory = function (req, res, callback) {
    
    homeModel.getHistory(req, res, function (err, activeApplications,pastApplications) {
        if (err) {
            callback(err);
        } else {
            
            callback(null, activeApplications,pastApplications);
        }
    })
}
//Wishlist controller
module.exports.getWishlist = function (req, res, callback) {
    
    homeModel.getWishlist(req, res, function (err, activeApplications,pastApplications) {
        if (err) {
            callback(err);
        } else {
            
            callback(null, activeApplications,pastApplications);
        }
    })
}

//Logout
module.exports.logout = function (req, res) {
    delete req.session.member_id;
    delete req.session.member_name;
    delete req.session.member_email;
    res.redirect('/login');
}




// Page View Cont
module.exports.viewCount = function (req, res, callback) {
    homeModel.viewCount(req, res, function (err, result) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);
            callback(null, result);
        }
    })
}


// Edit Profile
module.exports.edit = function (req, res, callback) {
    validate.edit(req, res, function (errors) {
        if (errors) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/profile');
        } else {
            req.session.success = true;
            homeModel.edit(req, res, function (err, result) {
                if (err) {
                    callback(err);
                } else {
                    if (result) {
                        // console.log("reslt cont...",result.value);
                        if(result.value.second_form) {
                            result = []
                            result.push({
                                msg: "Data Successfully Updated "
                            });
                            req.session.errors = false;
                            req.session.success = result;                                          
                            res.redirect('/profile');                           
                        }else{
                            res.send("true") 
                        }
                        
                    } else {
                        errors = []
                        errors.push({
                            msg: "Something Wrong. Data not Updated"
                        });
                        req.session.errors = errors;
                        req.session.success = false;
                        res.redirect('/profile');
                    }
                    //console.log(result);
                    callback(null, result);
                }
            })
        }
    });

}


// Payment
module.exports.payment = function (req, res, body) {
    req.session.success = true;
    homeModel.payment(req, res, body, function (err, result) {
        if (err) {
            throw (err);
        } else {
            if (result) {
                // console.log("in if");
                result = []
                result.push({
                    msg: "Data Successfully Done "
                });
                req.session.errors = false;
                req.session.success = result;
                res.redirect('/subcription');
            } else {
                errors = []
                errors.push({
                    msg: "Something Wrong. Data not Updated"
                });
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/subcription');
            }
        }
    })

}



// Forgot Password
module.exports.forgotPassword = function (req, res) {
    validate.forgotPassword(req, res, function (errors) {
        if (errors) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/forgotPassword');
        } else {
            req.session.success = true;
            homeModel.forgotPassword(req, res, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    
                    var registerdEmail = result.value.email
                    
                    if (result) {
                        result = []
                        result.push({
                            msg: "Your OTP has Send to Your Email("+ registerdEmail +")"
                        });
                        req.session.errors = false;
                        req.session.success = result;
                        req.session.registerdEmail = registerdEmail;
                        res.redirect('/updatePassword');
                    } else {

                        req.session.errors = [result];
                        req.session.success = false;
                        res.redirect('/forgotPassword');
                    }
                }
            })
        }
    });
}


module.exports.recruterRegistration = function (req, res) {
    validate.forgotPassword(req, res, function (errors) {
        if (errors) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/forgotPassword');
        } else {
            req.session.success = true;
            homeModel.recruterRegistration(req, res, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    //console.log(result);
                    if (result) {
                        result = []
                        result.push({
                            msg: "Please Login by Your Email"
                        });
                        req.session.errors = false;
                        req.session.success = result;
                        res.redirect('/recruter-registration');
                    } else {

                        req.session.errors = [result];
                        req.session.success = false;
                        res.redirect('/forgotPassword');
                    }
                }
            })
        }
    });
}


// update Password
module.exports.updatePassword = function (req, res) {
    validate.updatePassword(req, res, function (errors) {
        if (errors) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/updatePassword');
        } else {
            //  console.log(req.body.email);
            //  return
            req.session.success = true;
            homeModel.updatePassword(req, res, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    if (result) {
                        result = []
                        result.push({
                            msg: "Password Changed Successfully"
                        });
                        req.session.errors = false;
                        req.session.success = result;
                        res.redirect('/login');
                    } else {

                        req.session.errors = [result];
                        req.session.success = false;
                        res.redirect('/updatePassword');
                    }
                }
            })
        }
    });
}

//mail registration controller

module.exports.newRegistration = function (req, res) {
    validate.newRegistration(req, res, function (errors) {
        if (errors) {
            console.log(errors);
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/new-registration');
        } else {
            req.session.success = true;
            
            
            homeModel.newRegistration(req, res, function (err, result) {
                console.log(result.ops[0].email);
                var regiteredEmail = result.ops[0].email
                if (err) {
                    throw err;
                } else {
                    
                    if (result) {
                        result = []
                        result.push({
                            msg: "Your Login link has Send to Your Email address("+ regiteredEmail +")"
                        });
                        req.session.errors = false;
                        req.session.success = result;
                        req.session.registerdEmail = regiteredEmail;
                        res.redirect('/new-registration');
                    } else {

                        req.session.errors = [result];
                        req.session.success = false;
                        res.redirect('/new-registration');
                    }
                }
            })
        }
    });
}




// All notices
module.exports.getNotices = function (req, res, callback) {
    homeModel.getNotices(req, res, function (err, result) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);
            callback(null, result);
        }
    })
}


// All notices
module.exports.getNotice = function (req, res, callback) {
    homeModel.getNotice(req, res, function (err, result) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);
            callback(null, result);
        }
    })
}

//basic company details
module.exports.basicCompanyDetails= function(req, res){
    console.log("controller");
    console.log(req.body.title + " title..");
    validateCompanyDetails.basicCompanyDetails(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            
            res.redirect('/');
        }else{
            req.session.success=true; 
            console.log("controller 2")
            homeModel.basicCompanyDetails(req, res, function(err,result){
                //return
                if(err){
                    throw err;                    
                }else{
                    //console.log(result)
                    //console.log(result.ops[0].title)
                    var journal_title=result.ops[0].title;
                    
                        result=[]
                        result.push({msg: "Information added"});
                        req.session.errors=false;
                        req.session.success=result;
                    res.redirect('/company-info');                    
                }
            })     
        }
        
    });    
}

//job list controller
module.exports.jobList = function (req, res, callback) {

    homeModel.jobList(req, res, function (err, result,totaldata) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);
            callback(null, result,totaldata);
        }
    })
}

//filtered job list
module.exports.filteredJobList = function (req, res, callback) {

    homeModel.filteredJobList(req, res, function (err, result,totaldata) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);
            callback(null, result,totaldata);
        }
    })
}



module.exports.searchedJobs = function (req, res, callback) {
    homeModel.searchedJobs(req, res, function (err, result,totaldata) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);




            callback(null, result,totaldata);
        }
    })
}

module.exports.filteredJobs = function (req, res, callback) {
    homeModel.filteredJobs(req, res, function (err, result) {
        if (err) {
            callback(err);
        } else {
            res.send(result);

            //callback(null, result);
        }
    })
}

module.exports.searchedJobsGet = function (req, res, callback) {
    homeModel.searchedJobsGet(req, res, function (err, result) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);
            


            
            callback(null, result);
        }
    })
}



// Get latest notice
module.exports.getlatestNotice = function (req, res, callback) {
    homeModel.getlatestNotice(req, res, function (err, result) {
        if (err) {
            callback(err);
        } else {
            //console.log(result);
            callback(null, result);
        }
    })
}

// recaptcha
var SECRET = "6LcVc8YUAAAAAN7x3yjAY0w1x9N9EccERLogf4i_";

// Helper function to make API call to recatpcha and check response
function verifyRecaptcha(key, callback) {
    if (!key) {
        return callback(false);
    }

    https.get("https://www.google.com/recaptcha/api/siteverify?secret=" + SECRET + "&response=" + key, function (res) {
        // console.log(res);
        var data = "";
        res.on('data', function (chunk) {
            data += chunk.toString();
        });
        res.on('end', function () {
            try {
                var parsedData = JSON.parse(data);
                callback(parsedData.success);
            } catch (e) {
                callback(false);
            }
        });
    });

}