const validate = require('../utility/homeValidation.js');
const validateCompanyDetails = require('../utility/companyDetailsValidation.js');
const validateJobPostDetails = require('../utility/validateJobPostDeatails');
const jobPostModel = require('../model/recruiterCompModel');

var https = require('https');






module.exports.addNewJob= function(req, res){
    
    validateJobPostDetails.addNewJob(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;            
            res.redirect('/recruiter/jobPosts/addNew');
        }else{
            req.session.success=true; 
            console.log("controller 2")
            jobPostModel.addNewJob(req, res, function(err,result){
                //return
                if(err){
                    throw err;                    
                }else{
                    //console.log(result)
                    //console.log(result.ops[0].title)
                    var journal_title=result.ops[0].title;
                    
                        result=[]
                        result.push({msg: "Information added"});
                        req.session.errors=false;
                        req.session.success=result;
                    res.redirect('/recruiter/jobPosts/addNew');                    
                }
            })     
        }
        
    });    
}

module.exports.getJobPosted = function(req, res,callback){
    console.log(req.query.search);
    console.log("cont")

    jobPostModel.getJobPosted(req, res, function(err,result,dropDownData){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result,dropDownData);      
        }
    }) 

}    


module.exports.getCompany = function(req, res,callback){
    jobPostModel.getCompany(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result);       
        }
    }) 

}

module.exports.sortByData = function(req,res) {
    console.log(req.body.sortByCompany)
    let sortByCompany = req.body.sortByCompany;
    
        req.session.sortBydata = sortByCompany;
        console.log(req.session.sortBydata)
        

        res.redirect('/recruiter/jobposts/job-list?search='+sortByCompany);
    
}


//get company lists
module.exports.getCompanies = function(req, res,callback){

    jobPostModel.getCompanies(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result);      
        }
    }) 

}    
