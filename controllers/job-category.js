const validate = require('../utility/homeValidation.js');
const validateCompanyDetails = require('../utility/companyDetailsValidation.js');
const validateCategory = require('../utility/category-validation');
const jobCategoryModel = require('../model/job-category-model');

var https = require('https');

module.exports.addNewCategory= function(req, res){
    
    validateCategory.addNewCategory(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;            
            res.redirect('/recruiter/job-category/add');
        }else{
            req.session.success=true; 
            console.log("controller 2")
            jobCategoryModel.addNewCategory(req, res, function(err,result){
                //return
                if(err){
                    throw err;                    
                }else{
                    //console.log(result)
                    //console.log(result.ops[0].title)
                    var journal_title=result.ops[0].title;
                    
                        result=[]
                        result.push({msg: "Information added"});
                        req.session.errors=false;
                        req.session.success=result;
                        res.redirect('/recruiter/job-category/add');                    
                }
            })     
        }
        
    });    
}

module.exports.getJobCategory = function(req, res,callback){

    jobCategoryModel.getJobCategory(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result);       
        }
    }) 

} 

// Edit Category Controller
module.exports.editCategory = function(req, res, callback){ 
    validateCategory.editCategory(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/recruiter/job-category/edit?edit?category_id='+req.body.category_id);
        }else{
            req.session.success=true;  
            jobCategoryModel.editCategory(req, res, function(err,result){
                if(err){
                    throw(err);
                }else{
                    result=[]
                    result.push({msg: "Category Successfully Updated"});
                    req.session.errors=false;
                    req.session.success=result;
                    res.redirect('/recruiter/job-category/job-category-list');                                       
                }
            })     
        }        
    });              
}

//fetch edit
module.exports.editGetCategory= function(req, res, callback){ 
    jobCategoryModel.editGetCategory(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            console.log(result);
            callback(null, result);       
        }
    })                
}


//Delete Category
module.exports.deleteCategory = function(req, res,callback){

    jobCategoryModel.deleteCategory(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            res.redirect('/recruiter/job-category/job-category-list')      
        }
    }) 

}
