const validate = require('../utility/homeValidation.js');
const validateCompanyDetails = require('../utility/companyDetailsValidation.js');
const validateCategory = require('../utility/category-validation');
const jobApplicationModel = require('../model/job-application-model');

var https = require('https');




//job application list get
module.exports.getAllApplications = function(req, res,callback){

    jobApplicationModel.getAllApplications(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result);       
        }
    }) 

} 

module.exports.getCompanyApplications = function(req, res,callback){

    jobApplicationModel.getCompanyApplications(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result);       
        }
    }) 

} 

module.exports.viewApplicant = function(req, res,callback){

    jobApplicationModel.viewApplicant(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            callback(null, result);       
        }
    }) 

} 







// Edit Category Controller
module.exports.editCategory = function(req, res, callback){ 
    validateCategory.editCategory(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/recruiter/job-category/edit?edit?category_id='+req.body.category_id);
        }else{
            req.session.success=true;  
            jobCategoryModel.editCategory(req, res, function(err,result){
                if(err){
                    throw(err);
                }else{
                    result=[]
                    result.push({msg: "Category Successfully Updated"});
                    req.session.errors=false;
                    req.session.success=result;
                    res.redirect('/recruiter/job-category/job-category-list');                                       
                }
            })     
        }        
    });              
}

//fetch edit
module.exports.editGetCategory= function(req, res, callback){ 
    jobCategoryModel.editGetCategory(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            console.log(result);
            callback(null, result);       
        }
    })                
}


//Delete Category
module.exports.deleteCategory = function(req, res,callback){

    jobCategoryModel.deleteCategory(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            res.redirect('/recruiter/job-category/job-category-list')      
        }
    }) 

}
