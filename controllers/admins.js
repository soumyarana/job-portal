const validate=require('../utility/adminValidation.js');
const adminModel=require('../model/adminModel.js');


//Admin Registration
module.exports.registration= function(req, res){
    validate.registration(req, res, function(errors){
        // console.log(errors);
        // return;
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
        }else{
            req.session.success=true;  
            adminModel.registration(req, res, function(err,result){
                console.log(result.ops[0].email);
                var regiteredEmail = result.ops[0].email
                if (err) {
                    throw err;
                } else {
                    
                    if (result) {
                        result = []
                        result.push({
                            msg: "Your Login link has Send to Your Email address("+ regiteredEmail +")"
                        });
                        req.session.errors = false;
                        req.session.success = result;
                        req.session.registerdEmail = regiteredEmail;
                        res.redirect('/admin/registration');
                    } else {

                        req.session.errors = [result];
                        req.session.success = false;
                        res.redirect('/admin/registration');
                    }
                }
            })     
        }
        
    });    
}

//Admin profile
module.exports.profile= function(req, res){
    validate.profile(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/admin/profile');
        }else{
            req.session.success=true;  
            adminModel.profile(req, res, function(err,result){
                if(err){
                    throw(err);
                }else{
                    if(req.body.email==req.body.old_email){
                        if(result){
                            result=[]
                            result.push({msg: "Profile Successfully Updated"});
                            req.session.errors=false;
                            req.session.success=result;
                            res.redirect('/admin/profile');
                        }else{
                            console.log("profile logout....")
                            res.redirect('/admin/logout');
                        } 
                    }else{
                        res.redirect('/admin/logout');
                    }
                                       
                }
            })     
        }        
    });    
}

//Admin get profile
module.exports.getProfile= function(req, res, callback){ 
    adminModel.getProfile(req, res, function(err,result){
        if(err){
            callback(err);
        }{
            if(result){ 
                console.log(result)             
                callback(null, result);
            }else{
                console.log("logout")
                res.redirect('/admin/logout');
            }             
        }
    })                 
}

//Admin Login
module.exports.login= function(req, res){    
    validate.login(req, res, function(errors){
        if(errors){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/admin/login');
        }else{
            req.session.success=true;
            adminModel.login(req,res,function(err,result){
                
                if(err){
                    throw err;
                }else{
                    if(result==true){
                        
                        result = []
                                result.push({
                                    msg: "You Have Successfully Login "
                                });
                        req.session.errors = false;
                        req.session.success = result;
                        
                        res.redirect('/admin/profile'); 
                    }else{
                        console.log("Wrong")
                        req.session.errors=[result];
                        req.session.success=false;
                        res.redirect('/admin/login');
                    }
                }                
            })            
        }
    });    
}

//Admin Logout
module.exports.logout= function(req, res){
    delete req.session.user_id;
    delete req.session.first_name;
    delete req.session.last_name;
    delete req.session.phone;
    delete req.session.username;
    delete req.session.user_role;
    delete req.session.image;
    
    res.redirect('/admin/login');
}


module.exports.checkValidUser= function(req, res, callback){
    adminModel.getProfile(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            if(result){              
                callback(null, result);
            }else{
                callback(null);
            }             
        }
    })
}


// Forgot Password
module.exports.forgotPassword = function (req, res) {
    validate.forgotPassword(req, res, function (errors) {
        if (errors) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/admin/forgot-password');
        } else {
            req.session.success = true;
            adminModel.forgotPassword(req, res, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    console.log(result);
                    var registerdEmail = result.value.email
                    
                    if (result) {
                        result = []
                        result.push({
                            msg: "Your OTP has Send to Your Email"
                        });
                        req.session.errors = false;
                        req.session.success = result;
                        req.session.registerdEmail = registerdEmail;
                        res.redirect('/admin/update-password');
                    } else {

                        req.session.errors = [result];
                        req.session.success = false;
                        res.redirect('/admin/forgot-password');
                    }
                }
            })
        }
    });
}



// update Password
module.exports.updatePassword = function (req, res) {
    validate.updatePassword(req, res, function (errors) {
        if (errors) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/admin/update-password');
        } else {
            //  console.log(req.body.email);
            //  return
            req.session.success = true;
            adminModel.updatePassword(req, res, function (err, result) {
                if (err) {
                    throw err;
                } else {
                    if (result) {
                        result = []
                        result.push({
                            msg: "Password Changed Successfully"
                        });
                        req.session.errors = false;
                        req.session.success = result;
                        res.redirect('/admin/login');
                    } else {

                        req.session.errors = [result];
                        req.session.success = false;
                        res.redirect('/admin/update-password');
                    }
                }
            })
        }
    });
}