//const validate=require('../utility/ManuscriptValidation.js');
const dashboardModel=require('../model/dashboardModel.js');




// For visite count
module.exports.getDashboard= function(req, res, callback){ 
    dashboardModel.getDashboard(req, res, function(err,result){
        if(err){
            callback(err);
        }{
            //console.log(result);
            callback(null, result);       
        }
    })                
}


// For member count
module.exports.getMembers= function(req, res, callback){ 
    dashboardModel.getMembers(req, res, function(err,result){
        if(err){
            callback(err);
        }{
            //console.log(result);
            callback(null, result);       
        }
    })                
}


// For Journal count
module.exports.getJournals= function(req, res, callback){ 
    dashboardModel.getJournals(req, res, function(err,result){
        if(err){
            callback(err);
        }{
            //console.log(result);
            callback(null, result);       
        }
    })                
}