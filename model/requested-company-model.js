const express=require('express');
var expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue

var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="companiesCollection"; // Define Collection Name

//Get App
const app= express.Router();
app.use(expressValidator())


//Job Category Deatils




//Get 
module.exports.getAllApplications = function(req, res, callback){
    var user_id = sessionValue.user_id;  
    console.log("......."+ req.session.user_id)
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).find({"companyAuthenticated": false}).toArray(function (err, jobApplicationData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(jobApplicationData){ 
                        
                        callback(null, jobApplicationData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}


module.exports.viewApplicant = function(req, res, callback){
    console.log(req.query.company_id)
    let company_id = req.query.company_id
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({"company_id":parseInt(company_id) }, function (err, applicantData) {
                console.log("applicantData",applicantData);
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(applicantData){ 
                        console.log("applicantData",applicantData);
                        callback(null, applicantData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}




//Edit Category
module.exports.rejectedCompany= function(req, res, callback){
    console.log("...../.....",req.body.rejectionCause)
    let rejectionCause = req.body.rejectionCause;
    console.log(req.query.companyEmail)
    

    
    
    connectDb(function (err, client) {
        if (err) {
          throw err;
        } else {
          const db = client.db(databaseName); // Enter Database Name
          //console.log(req.body.otp);
          //db.collection("forgotPassword").findOne({ otp:req.body.otp }, function(err,result){
          db.collection(collectionName).findOneAndUpdate({
            "companyEmail": req.query.companyEmail
          },{
            $set:{
              "companyRejected" : true,
              "rejectionCause" : rejectionCause                
            }
          },{
            upsert:true
          }, function (err, result) {
            console.log("/////....",result)
            client.close();
            if (!result) {
              var error = {
                param: "otp",
                msg: "Your OTP does not Match",
                value: req.body.otp
              };
              if (!errors) {
                errors = [];
              }
              errors.push(error);
            } else {
              var nodemailer = require('nodemailer');
              var smtpTransport = require('nodemailer-smtp-transport');

              var mailAccountUser = 'testsmtptt@gmail.com'
              var mailAccountPassword = 'testsmtppassword'
              //var mailAccountPassword =process.env.GMAIL_PASS

              var fromEmailAddress = 'testsmtptt@gmail.com'
              var toEmailAddress = req.query.companyEmail

              var transport = nodemailer.createTransport({
                  service: 'gmail',
                  auth: {
                      user: mailAccountUser,
                      pass: mailAccountPassword
                  }
              })

              var mail = {
                  from: fromEmailAddress,
                  to: toEmailAddress,
                  subject: "Selection Mail",
                  text: "Job Portal",
                  html: "Your requested company is approved by Job Portal"
              }

              transport.sendMail(mail, function(error, response){
                  if(error){
                      console.log(error);
                  }else{
                      console.log("Message sent: ");
                      //console.log(response);
                  }

                  transport.close();
              })
              
              callback(null, result);
              

            }
          })
              // Here need  to be next part of action - not outside find callback!
          // cb(errors)
        }
    })
}


//Edit fetch category
module.exports.editGetCategory= function(req, res, callback){
    var category_id = req.query.category_id; 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({category_id:parseInt(category_id)}, function (err, categoryData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(categoryData){ 
                        console.log(categoryData);
                        callback(null, categoryData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

//Delete Category
module.exports.deleteCategory = function(req, res, callback){
    var category_id = req.query.category_id; 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOneAndDelete({category_id:parseInt(category_id)}, function (err, categoryData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(categoryData){ 
                        console.log(categoryData);
                        callback(null, categoryData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}