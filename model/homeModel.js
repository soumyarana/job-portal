const express=require('express');
const expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue
const cryptoRandomString = require('crypto-random-string');

var multer = require('multer');
const fs = require('fs');
const utils = require('../helpers/utility')

//const documentImage = require('../public/images/uploads/noImage.png');

const databaseName=configRoute.databaseName;// Store Database Name
const collectionName="member1"; // Define Collection Name


//Get App
const app= express.Router();
app.use(expressValidator())



// Member Registration
module.exports.registration= function(req, res, callback){
    req.collectionName=collectionName; // Enter Collection Name For Create Collection Index
    collectionIndex.checkIndex(req, res, function(result){
        bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
            if(err){
                throw err;
            }else{
                let now = new Date();
                
                var item={
                    member_id:result,
                    name:req.body.name,
                    email:req.body.email,
                    alt_email:req.body.alt_email,
                    password:hash,
                    nationality:req.body.nationality,
                    user_role:2,
                    image:"../public/images/uploads/noImage.png",
                    education_qualification:req.body.education_qualification,
                    home_address:req.body.home_address,
                    pin_number:req.body.pin_number,
                    personal_number:req.body.personal_number,
                    created_ip_address:req.connection.remoteAddress,
                    modified_ip_address:req.connection.remoteAddress,
                    created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
                    modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
                }
                connectDb(function(err, client){
                    if(err){
                        throw err;
                    }else{
                        const db=client.db(databaseName); // Enter Database Name
                        db.collection(collectionName).insertOne(item, function(err,result){
                            client.close();
                            if(err){
                                throw err;
                            }else{
                                callback(null, result);
                            }
                            //client.close();
                        })
                    }                
                });
            }
        });                    
    })  
}


// Member Login
module.exports.login= function(req, res, callback){
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({ email: req.body.email }, function(err,result){  
                client.close();   
                if(err){
                    
                    req.session.error = "Email is not registered"
                    throw err;
                    
                }else{
                    if(result){
                        bcrypt.compare(req.body.password, result.password, function(err, isMatch) {
                            if(err){
                                throw err;
                                
                            }else{
                                if(isMatch){
                                    sessionValue=req.session;
                                    sessionValue.member_id=result.member_id;
                                    //sessionValue.member_name=result.name;
                                    sessionValue.member_email=result.email;
                                    sessionValue.member_image=result.image;
                                    sessionValue.fname=result.fname;
                                    sessionValue.lname=result.lname;

                                    console.log(sessionValue.member_image);

                                    callback(null, isMatch);
                                }else{
                                    callback(null,{msg: "password not match"});
                                }                                    
                            }
                        }); 
                    }else{
                       callback(null, {msg: "Incorrect Email"} );
                    }                                                                     
                }
            });
        }            
    });
}

// Add Basic company details
module.exports.basicCompanyDetails = function(req, res, callback){
    console.log("model...");
    req.collectionName=collectionName; // Enter Collection Name For Create Collection Index
    console.log(req.body.companyName +"check..")
    collectionIndex.checkIndex(req, res, function(result){
        let now = new Date();
        var item={
            companyName: req.body.companyName,
            title:req.body.title,
            location:req.body.location,            
            created_ip_address:req.connection.remoteAddress,
            modified_ip_address:req.connection.remoteAddress,
            created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            status:parseInt(req.body.status)
        }
        connectDb(function(err, client){
            if(err){
                throw err;
            }else{
                const db=client.db(databaseName); // Enter Database Name
                db.collection(collectionName).insertOne(item, function(err,result){
                    client.close();
                    if(err){
                        throw err;
                    }else{
                        console.log(result);
                        callback(null, result);
                    }
                    
                })
            }                
        });
                                
    })  
}


// Get Member Profile Value
module.exports.getProfile= function(req, res, callback){
    sessionValue=req.session;
    var member_id=sessionValue.member_id;  
    // console.log(sessionValue.member_image);
    // var member_id=4;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({member_id:member_id},{projection:{"password":0,"token":0,"otp":0}}, function (err, memberData) {
                
                if(err){
                    callback(err);
                }else{     
                    if(memberData){ 
                        connectDb(function(err, client){
                            if(err){
                                throw err;
                            }else{
                                const db=client.db(databaseName); // Enter Database Name
                                db.collection("countries").find().toArray(function (err, noticeData) {
                                    client.close();
                                    if(err){
                                        callback(err);
                                    }else{                   
                                        //console.log(noticeData);
                                        //return;
                                        if(noticeData){ 
                                            
                                            callback(null, memberData,noticeData);


                                            //callback(null, noticeData);                        
                                        }else{
                                            callback(null);
                                        }
                                    }
                                });
                            }                
                        });


                        
                                                
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

//get member dashboard
module.exports.getDashboard= function(req, res, callback){
    sessionValue=req.session;
    var member_id=sessionValue.member_id;  
    // console.log(sessionValue.member_image);
    // var member_id=4;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("memberHistoryCollection").count({member_id:parseInt(member_id)},function(err,appliesCount) {
                if(err) {
                    callback(err)
                }else{
                    db.collection("memberSaveJobCollection").count({member_id:parseInt(member_id)},function(err,saveJobsCount) {
                        if(err) {
                            callback(err)
                        }else{
                            db.collection("memberHistoryCollection").count({member_id:parseInt(member_id),"notSelected":true},function(err,pendingCount) {
                                if(err) {
                                    callback(err)
                                }else{

                                    db.collection("memberHistoryCollection").count({member_id:parseInt(member_id),"notSelected":false},function(err,selectionCount) {
                                        if(err) {
                                            callback(err)
                                        }else{
                                            let applicationCount = {
                                                "totalApplications":appliesCount,
                                                "pendingApplications":pendingCount,
                                                "selectApplications":selectionCount
                                            }
        
                                            callback(null,applicationCount,saveJobsCount)
                                        }
                                    })
                                }
                            })

                        }
                    })
                }
                
            })

        }                
    });
}

//get member history
module.exports.getHistory= function(req, res, callback){
    sessionValue=req.session;
    var member_id=sessionValue.member_id;  

    
    let x = new Date();
    x.setDate(x.getDate() - 10)
       
    // let exp =  x.setDate(x.getDate() - 7); 
    let expiaryDate = date.format(x, 'YYYY/MM/DD HH:mm:ss');

    // console.log(sessionValue.member_image);
    // var member_id=4;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("jobPostCollection").find({"created_date": { $gt: expiaryDate},"applications":{"$elemMatch":{"applicant_id":req.session.member_id}}}).toArray(function(err,activeApplications){
                console.log("mmmmmmmmm")
                console.log("history.........",activeApplications)
                if(err) {
                    callback(err)
                }else{
                    db.collection("jobPostCollection").find({created_date: { $lt: expiaryDate},"applications":{"$elemMatch":{"applicant_id":req.session.member_id}}}).toArray(function(err,pastApplications){

                        if(err) {
                            callback(err)
                        }else{
                            // for(let i = 0; i<=activeApplications.length-1;i++) {
                            //     let eachJob = activeApplications[i]
                            //     if(activeApplications[i].applications) {
                            //         let applicationArray = activeApplications[i].applications
                                
                                   
                            //     for(let y = 0; y<=applicationArray.length-1;y++ ) {
                            //         console.log("applicant id ....",applicationArray[y].applicant_id);
                            //         if(applicationArray[y].applicant_id == req.session.member_id) {
                            //             activeApplications[i].appliedCandidate = true;
                            //         }

                            //     }
                            //     }
                                
                            // }

                            //for save btn

                            for(let i = 0; i<=activeApplications.length-1;i++) {
                                let eachJob = activeApplications[i]
                                if(activeApplications[i].saveJobs) {
                                    let saveJobArray = activeApplications[i].saveJobs
                                
                                   
                                for(let y = 0; y<=saveJobArray.length-1;y++ ) {
                                    console.log("applicant id ....",saveJobArray[y].applicant_id);
                                    if(saveJobArray[y].applicant_id == req.session.member_id) {
                                        activeApplications[i].jobSaved = true;
                                    }

                                }
                                }
                                
                            }
                            db.collection("jobPostCollection").find({"created_date": { $gt: expiaryDate},"applications":{"$elemMatch":{"applicant_id":req.session.member_id}}}).toArray(function(err,totalActiveApplications) {
                                if(err) {
                                    callback(err)
                                }else{
                                    
                                    //page portion........
                                    // let numOfPage = totalActiveApplications.length / limitValue
                                    // let pageno = Math.ceil(numOfPage)
                                    // console.log(pageno);
                                    // console.log(Array(pageno).fill("aa"))
                                    // let myArray = Array(pageno).fill("aa");
                                    
                                    // activeApplications["pageArray"] = myArray
                                    // console.log("..............",activeApplications.pageArray);

                                   
                                    // console.log("history.........",activeApplications)
                                     callback(null,activeApplications,pastApplications);

                                }
                            })

                        }
                        })
                }
                
            })

        }                
    });
}

//get member history
module.exports.getWishlist= function(req, res, callback){
    sessionValue=req.session;
    var member_id=sessionValue.member_id;  

    
    let x = new Date();
    x.setDate(x.getDate() - 10)
       
    // let exp =  x.setDate(x.getDate() - 7); 
    let expiaryDate = date.format(x, 'YYYY/MM/DD HH:mm:ss');

    // console.log(sessionValue.member_image);
    // var member_id=4;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("jobPostCollection").find({"created_date": { $gt: expiaryDate},"saveJobs":{"$elemMatch":{"applicant_id":req.session.member_id}}}).toArray(function(err,activeApplications){
                console.log("mmmmmmmmm")
                console.log("history.........",activeApplications)
                if(err) {
                    callback(err)
                }else{
                    db.collection("jobPostCollection").find({created_date: { $lt: expiaryDate},"saveJobs":{"$elemMatch":{"applicant_id":req.session.member_id}}}).toArray(function(err,pastApplications){

                        if(err) {
                            callback(err)
                        }else{
                            for(let i = 0; i<=activeApplications.length-1;i++) {
                                let eachJob = activeApplications[i]
                                if(activeApplications[i].applications) {
                                    let applicationArray = activeApplications[i].applications
                                
                                   
                                for(let y = 0; y<=applicationArray.length-1;y++ ) {
                                    console.log("applicant id ....",applicationArray[y].applicant_id);
                                    if(applicationArray[y].applicant_id == req.session.member_id) {
                                        activeApplications[i].appliedCandidate = true;
                                    }

                                }
                                }
                                
                            }

                            //for save btn

                            for(let i = 0; i<=activeApplications.length-1;i++) {
                                let eachJob = activeApplications[i]
                                if(activeApplications[i].saveJobs) {
                                    let saveJobArray = activeApplications[i].saveJobs
                                
                                   
                                for(let y = 0; y<=saveJobArray.length-1;y++ ) {
                                    console.log("applicant id ....",saveJobArray[y].applicant_id);
                                    if(saveJobArray[y].applicant_id == req.session.member_id) {
                                        activeApplications[i].jobSaved = true;
                                    }

                                }
                                }
                                
                            }
                            db.collection("jobPostCollection").find({"created_date": { $gt: expiaryDate},"applications":{"$elemMatch":{"applicant_id":req.session.member_id}}}).toArray(function(err,totalActiveApplications) {
                                if(err) {
                                    callback(err)
                                }else{
                                    
                                    //page portion........
                                    // let numOfPage = totalActiveApplications.length / limitValue
                                    // let pageno = Math.ceil(numOfPage)
                                    // console.log(pageno);
                                    // console.log(Array(pageno).fill("aa"))
                                    // let myArray = Array(pageno).fill("aa");
                                    
                                    // activeApplications["pageArray"] = myArray
                                    // console.log("..............",activeApplications.pageArray);

                                   
                                    // console.log("history.........",activeApplications)
                                     callback(null,activeApplications,pastApplications);

                                }
                            })

                        }
                        })
                }
                
            })

        }                
    });
}


// Edit Profile 
module.exports.edit= function(req, res, callback){
    console.log("model exp..",req.body.experience)

    
    console.log("000000 "+req.body.hm_house_no);
    console.log("0000001 "+req.body.hm_country_name);

    let now = new Date();
    var member_id=req.body.member_id;
    

    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name

            db.collection(collectionName).findOne({member_id:parseInt(member_id)}, function (err, memberData) {
                if(err){
                    callback(err);
                }else{
                    let now = new Date();
                    if(req.body.first_form === "true") {  
                        if(req.file){
                            // const appPath = utils.appPath();
                            //  const path = req.file.path.replace(appPath, "");
                            //  req.body.image = path;
                            var imageNew = req.file.filename;
                        }else{
                            var imageNew = memberData.image;
                        }
                                               
                    }else {
                        if(req.file){
                            // const appPath = utils.appPath();
                            //  const path = req.file.path.replace(appPath, "");
                            //  req.body.image = path;
                            var image_cv = req.file.filename;
                        }else{
                            var image_cv = memberData.upload_cv;
                        }
                        
                    }
                    if(req.body.experience) {
                        let expArr = req.body.experience;
                        let expCompArr = req.body.company;
                        let designationArr = req.body.designation;
                       var expreiences = [];
                        for(let x= 0;x<expArr.length-1;x++) {
                            let experience = expArr[x];
                            let company = expCompArr[x];
                            let designation = designationArr[x];
                            expreiences.push({"experience":experience,"company":company,"designation":designation})
                        }

                    }else{
                        var expreiences = [];
                    }

                    if(memberData){
                        // db.collection("countries").findOne({"id":parseInt(req.body.hm_country)},function (err, noticeData) {
                            
                        //     if(err){
                        //         callback(err);
                        //     }else{
                        //         console.log(noticeData);                   
                        //        nameCountry = noticeData.name
                        //     }
                        // });
                        //console.log("hdsjdvsv",nameCountry);
                        if(req.body.first_form === "true") {
                            
                        db.collection("countries").findOne({"id":parseInt(req.body.hm_country),},function (err, noticeData) {
                            
                            if(err){
                                callback(err);
                            }else{
                                               
                               let nameCountry = noticeData.name       
                        let image = imageNew ;

                        let mb_no = req.body.mb_no;
                        let p_house_no = req.body.p_house_no;
                        let p_street = req.body.p_street;
                        let p_city = req.body.p_city;
                        let p_state = req.body.p_state;
                        let p_pin = req.body.p_pin;

                        let hm_house_no = req.body.hm_house_no;
                        let hm_street = req.body.hm_street;
                        let hm_city = req.body.hm_city;
                        let hm_state = req.body.hm_state;
                        let hm_pin = req.body.hm_pin;
                        let hm_country = nameCountry ;
                        let hm_country_id = req.body.hm_country;
                        let hm_state_nw = req.body.hm_state2;
                                           

                        

                        let status=parseInt(req.body.status);           
                        let available_reviewers=req.body.available_reviewers;
                        let modified_ip_address=req.connection.remoteAddress;
                        let modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');        
                        
                        db.collection(collectionName).findOneAndUpdate({member_id:parseInt(member_id)}, { "$set": 
                        { 
                        "image":image,
                        "mb_no":mb_no,
                        "parmanent_address":{
                            "p_house_no":p_house_no,
                            "p_street":p_street,
                            "p_city":p_city,
                            "p_state":p_state,
                            "p_pin":p_pin

                        },
                        
                        "home_address":{
                            "hm_house_no":hm_house_no,
                            "hm_street":hm_street,
                            "hm_city":hm_city,
                            "hm_state":hm_state,
                            "hm_pin":hm_pin,
                            "hm_country_name":hm_country, 
                            "hm_country_id": hm_country_id,                          
                            "hm_state2":hm_state_nw
                            

                        },
                        "available_reviewers": available_reviewers,
                        "modified_ip_address": modified_ip_address, 
                        "modified_date": modified_date
                        }
                        },{ 
                            returnOriginal: false,
                            projection:{"password":0,"token":0,"otp":0}
                        },function (err, updateData)
                        {
                            client.close();
                            if(err){
                                callback(err);
                            }else{   
                                
                                sessionValue= req.session;
                                sessionValue.member_image = updateData.value.image;     
                                callback(null, updateData);   
                            }
                        }); 
                        }
                        })   
                        }else{  
                                                     
                        let cv_image = image_cv;
                        let skills = req.body.skills;
                        let degree = req.body.degree;
                        let college = req.body.college;
                        let pass_year = req.body.pass_year;
                        let experience = expreiences;
                        let company = req.body.company;
                        let degignation = req.body.degignation;
                        let about = req.body.about;
                                  
                        let available_reviewers=req.body.available_reviewers;
                        let modified_ip_address=req.connection.remoteAddress;
                        let modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');        
                        
                        db.collection(collectionName).findOneAndUpdate({member_id:parseInt(member_id)}, { "$set": 
                        {                         
                        "upload_cv":cv_image,
                        "skills":skills,
                        "degree":degree,
                        "college":college,
                        "pass_year":pass_year,
                        "experience":experience,
                        "company":company,
                        "degignation":degignation,
                        "about":about,

                        "available_reviewers": available_reviewers,
                        "modified_ip_address": modified_ip_address, 
                        "modified_date": modified_date
                        }
                        },{ 
                            returnOriginal: false,
                            projection:{"password":0,"token":0,"otp":0}
                        },function (err, updateData)
                        {
                            client.close();
                            if(err){
                                callback(err);
                            }else{  
                                
                                updateData.value["second_form"] = true;
                                sessionValue= req.session;
                                sessionValue.cv_image = updateData.value.upload_cv;    
                                callback(null, updateData);   
                            }
                        });
                    }
    
                    }else{
                        callback(null);
                    }                                        
                }
            });            
        }                
    });
}


module.exports.edit1= function(req, res, callback){
    
    console.log("000000 "+req.body.member_id)
    let now = new Date();
    var member_id=req.body.member_id;

    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({member_id:parseInt(member_id)}, function (err, memberData) {
                if(err){
                    callback(err);
                }else{
                    let now = new Date();
                    console.log("////..........",req.files.length)
                    
                    if(req.files.length>0) {
                        if(req.files[0].filename){
                            var userImage = req.files[0].filename
                        }else{
                            userImage = memberData.image
                        }
    
                        if(req.files[1]){
                            var image_cv = req.files[1].filename
                           
                        }else{
                            image_cv = memberData.upload_cv
                            
                        }
                    }else{
                        userImage = memberData.image;
                        image_cv = memberData.upload_cv;
                    }
                    

                    if(memberData){
                        
                        
                        let image = userImage ;

                        let mb_no = req.body.mb_no;
                        let p_house_no = req.body.p_house_no;
                        let p_street = req.body.p_street;
                        let p_city = req.body.p_city;
                        let p_state = req.body.p_state;
                        let p_pin = req.body.p_pin;

                        let hm_house_no = req.body.hm_house_no;
                        let hm_street = req.body.hm_street;
                        let hm_city = req.body.hm_city;
                        let hm_state = req.body.hm_state; 
                        let hm_pin = req.body.hm_pin;                       

                        let alt_email=req.body.alt_email;

                        let cv_image = image_cv;
                        let skills = req.body.skills;
                        let degree = req.body.degree;
                        let college = req.body.college;
                        let pass_year = req.body.pass_year;
                        let experience = req.body.experience;
                        let company = req.body.company;
                        let degignation = req.body.degignation;
                        let about = req.body.about;

                        let status=parseInt(req.body.status);           
                        let available_reviewers=req.body.available_reviewers;
                        let modified_ip_address=req.connection.remoteAddress;
                        let modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');        
                        
                        db.collection(collectionName).findOneAndUpdate({member_id:parseInt(member_id)}, { "$set": 
                        { 
                        "image":image,
                        "mb_no":mb_no,
                        "parmanent_address":{
                            "p_house_no":p_house_no,
                            "p_street":p_street,
                            "p_city":p_city,
                            "p_state":p_state,
                            "p_pin":p_pin

                        },
                        "alt_email": alt_email,
                        "home_address":{
                            "hm_house_no":hm_house_no,
                            "hm_street":hm_street,
                            "hm_city":hm_city,
                            "hm_state":hm_state,
                            "hm_pin":hm_pin

                        },

                        "upload_cv":cv_image,
                        "skills":skills,
                        "degree":degree,
                        "college":college,
                        "pass_year":pass_year,
                        "experience":experience,
                        "company":company,
                        "degignation":degignation,
                        "about":about,

                        "available_reviewers": available_reviewers,
                        "modified_ip_address": modified_ip_address, 
                        "modified_date": modified_date
                        }
                        },{ 
                            returnOriginal: false,
                            projection:{"password":0,"token":0,"otp":0}
                        },function (err, updateData)
                        {
                            client.close();
                            if(err){
                                callback(err);
                            }else{   
                                
                                sessionValue= req.session;
                                sessionValue.member_image = updateData.value.image;     
                                callback(null, updateData);   
                            }
                        }); 

                        
    
                    }else{
                        callback(null);
                    }                                        
                }
            });            
        }                
    });
}








// Get View Count
module.exports.viewCount= function(req, res, callback){  
    var fieldName="visiteCount"        
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName);
            db.collection("viewCount").findOneAndUpdate({"visiteCount": fieldName}, {$inc: { last_count: 1}},
                function (err, visiteData) { 
                    //client.close();                           
                if(err){
                    callback(err);
                }else{              
                    if(visiteData.value){
                        //console.log(visiteData.value.last_count)
                        callback(visiteData.value.last_count+1)
                    }else{
                        var item={
                            visiteCount:fieldName,
                            last_count:1
                            }
                        const db=client.db(databaseName);
                        db.collection("viewCount").insertOne(item, function(err,result){
                            client.close();
                            if(err){
                                callback(err);
                            }else{
                                //console.log(result.ops[0].last_count);
                                callback(result.ops[0].last_count);
                            }
                        })
                    }
                }
                client.close(); 
            });
        }
    });    
}



// payment portion
module.exports.payment= function(req, res, body, callback){

    var member_id=sessionValue.member_id;
    var razorpay_payment_id=req.params.razorpay_payment_id;
    

    req.collectionName="payments"; // Enter Collection Name For Create Collection Index
    collectionIndex.checkIndex(req, res, function(result){
        
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            let now = new Date();
            var modified_ip_address= req.connection.remoteAddress;
            var modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');
            var item={
                payment_id:result,
                razorpay_payment_id:razorpay_payment_id,               
                user_role:2,
                other_details:body, 
                member_id:member_id,        
                created_ip_address:req.connection.remoteAddress,
                modified_ip_address:req.connection.remoteAddress,
                created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
                modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
            }
            connectDb(function(err, client){
                if(err){
                    throw err;
                }else{
                    const db=client.db(databaseName); // Enter Database Name
                    db.collection("payments").insertOne(item, function(err,result){
                        //client.close();
                        if(err){
                            throw err;
                        }else{
                            //console.log(result);
                            //callback(null, result);
                            if(result){
                                //console.log("in function");
                                //console.log(member_id);
                                db.collection(collectionName).findOneAndUpdate({member_id:parseInt(member_id)}, { "$set": {
                                     "flag": parseInt(1),                                   "modified_ip_address": modified_ip_address, 
                                     "modified_date": modified_date
                                        }
                                },{ returnOriginal: false},function (err, updateData)
                                {
                                    client.close();
                                    if(err){
                                        callback(err);
                                    }else{   
                                        //console.log(updateData)    
                                        callback(null, result);   
                                    }
                                });
                            }else{
                                callback(null);
                            }
                        }
                        //client.close();
                    })
                }                
            });
        }       
    })            
    })  
}



// Forgot Password
// module.exports.forgotPassword= function(req, res, callback){

//     var rn = require('random-number');
//     var options = {
//       min:  100000
//     , max:  900000
//     , integer: true
//     }
//     var otp=rn(options);

//     req.collectionName="forgotPassword"; // Enter Collection Name For Create Collection Index
//     collectionIndex.checkIndex(req, res, function(result){
        
//         connectDb(function(err, client){
//         if(err){
//             throw err;
//         }else{
//             let now = new Date();
//             var item={
//                 forgot_password_id:result,       
//                 email:req.body.email,
//                 otp:otp,
//                 created_ip_address:req.connection.remoteAddress,
//                 modified_ip_address:req.connection.remoteAddress,
//                 created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
//                 modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
//             }
            
            
//             const db=client.db(databaseName); // Enter Database Name
//                     db.collection("forgotPassword").insertOne(item, function(err,result){
//                         client.close();
//                         if(err){
//                             throw err;
//                         }else{
//                             //console.log(result);

//                             var nodemailer = require('nodemailer');
//                             var smtpTransport = require('nodemailer-smtp-transport');
            
//                             var mailAccountUser = 'testsmtptt@gmail.com'
//                             var mailAccountPassword = 'testsmtppassword'
//                             //var mailAccountPassword =process.env.GMAIL_PASS
            
//                             var fromEmailAddress = 'testsmtptt@gmail.com'
//                             var toEmailAddress = req.body.email
            
//                             var transport = nodemailer.createTransport({
//                                 service: 'gmail',
//                                 auth: {
//                                     user: mailAccountUser,
//                                     pass: mailAccountPassword
//                                 }
//                             })
            
//                             var mail = {
//                                 from: fromEmailAddress,
//                                 to: toEmailAddress,
//                                 subject: "Change Password",
//                                 text: "Univerisity",
//                                 html: "Your OTP  is: <br><p><b>"+otp+"</b></p>"
//                             }
            
//                             transport.sendMail(mail, function(error, response){
//                                 if(error){
//                                     console.log(error);
//                                 }else{
//                                     console.log("Message sent: ");
//                                     //console.log(response);
//                                 }
            
//                                 transport.close();
//                             })
                            
//                             callback(null, result);
//                         }
//                         //client.close();
//                     })
//         }       
//     })            
//     })  
// }
//forgot password model
module.exports.forgotPassword= function(req, res, callback){

    var rn = require('random-number');
    var options = {
      min:  100000
    , max:  900000
    , integer: true
    }
    var otp=rn(options);

    req.collectionName="forgotPassword"; // Enter Collection Name For Create Collection Index
    collectionIndex.checkIndex(req, res, function(result){
        
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            let now = new Date();
            
            
            
            const db=client.db(databaseName); // Enter Database Name
                    db.collection("forgotPassword").findOneAndUpdate({
                        "email" : req.body.email
                    },{
                        $set : {
                            "otp":otp
                        }
                    },
                    {
                        upsert:true
                    },function(err,result){
                        client.close();
                        if(err){
                            throw err;
                        }else{
                            //console.log(result);

                            var nodemailer = require('nodemailer');
                            var smtpTransport = require('nodemailer-smtp-transport');
            
                            var mailAccountUser = 'testsmtptt@gmail.com'
                            var mailAccountPassword = 'testsmtppassword'
                            //var mailAccountPassword =process.env.GMAIL_PASS
            
                            var fromEmailAddress = 'testsmtptt@gmail.com'
                            var toEmailAddress = req.body.email
            
                            var transport = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: mailAccountUser,
                                    pass: mailAccountPassword
                                }
                            })
            
                            var mail = {
                                from: fromEmailAddress,
                                to: toEmailAddress,
                                subject: "Change Password",
                                text: "Job Portal",
                                html: "Your OTP  is: <br><p><b>"+otp+"</b></p>"
                            }
            
                            transport.sendMail(mail, function(error, response){
                                if(error){
                                    console.log(error);
                                }else{
                                    console.log("Message sent: ");
                                    //console.log(response);
                                }
            
                                transport.close();
                            })
                            
                            callback(null, result);
                        }
                        //client.close();



                    }
                        
                    )
        }       
    })            
    })  
}




//mail registration
module.exports.newRegistration= function(req, res, callback){
     let token = cryptoRandomString({length: 25, type: 'hex'})
    console.log(token);
    let firstName = req.body.fname;
    let f_name = firstName[0].toUpperCase() + firstName.slice(1);

    let lastName = req.body.lname;
    let l_name = lastName[0].toUpperCase() + lastName.slice(1);

    // console.log(urlToMail + " urlToMail");
    let rn = require('random-number');
    let options = {
      min:  100000
    , max:  900000
    , integer: true
    }
    let otp=rn(options);
    let urlToMail = `http://localhost:5000/new-registration/${token}`;

    //req.collectionName="mailRegistration"; // Enter Collection Name For Create Collection Index
    req.collectionName=collectionName;
    collectionIndex.checkIndex(req, res, function(result){
        bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            let now = new Date();
            var item={
                member_id:result,
                fname:f_name,
                lname:l_name,       
                email:req.body.email,
                password:hash,
                token:token,
                emailVerified : "false",
                //image: documentImage,
                created_ip_address:req.connection.remoteAddress,
                modified_ip_address:req.connection.remoteAddress,
                created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
                modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
            }
            
            
            const db=client.db(databaseName); // Enter Database Name
                    db.collection(collectionName).insertOne(item, function(err,result){
                        client.close();
                        if(err){
                            throw err;
                        }else{
                            //console.log(result);

                            var nodemailer = require('nodemailer');
                            var smtpTransport = require('nodemailer-smtp-transport');
            
                            var mailAccountUser = 'testsmtptt@gmail.com'
                            var mailAccountPassword = 'testsmtppassword'
                            //var mailAccountPassword =process.env.GMAIL_PASS
            
                            var fromEmailAddress = 'testsmtptt@gmail.com'
                            var toEmailAddress = req.body.email
            
                            var transport = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: mailAccountUser,
                                    pass: mailAccountPassword
                                }
                            })
            
                            let mail = {
                                from: fromEmailAddress,
                                to: toEmailAddress,
                                subject: "Email Verify",
                                text: "Job Portal",
                                html: `Please click the link to verify the email:<a href = "${urlToMail}">login link click here...</a>`
                            }
            
                            transport.sendMail(mail, function(error, response){
                                if(error){
                                    console.log(error);
                                }else{
                                    console.log("Message sent: ");
                                    //console.log(response);
                                }
            
                                transport.close();
                            })
                            console.log(result.ops[0].email)
                            callback(null, result);
                        }
                        //client.close();
                    })
                    
        }       
    }) 
    });           
    })  
}

// Forgot Password
module.exports.recruterRegistration= function(req, res, callback){

    var rn = require('random-number');
    var options = {
      min:  100000
    , max:  900000
    , integer: true
    }
    var otp=rn(options);

    req.collectionName="recruterRegistration"; // Enter Collection Name For Create Collection Index
    collectionIndex.checkIndex(req, res, function(result){
        
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            let now = new Date();
            var item={
                forgot_password_id:result,       
                email:req.body.email,
                otp:otp,
                created_ip_address:req.connection.remoteAddress,
                modified_ip_address:req.connection.remoteAddress,
                created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
                modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
            }
            
            
            const db=client.db(databaseName); // Enter Database Name
                    db.collection("recruterRegistration").insertOne(item, function(err,result){
                        client.close();
                        if(err){
                            throw err;
                        }else{
                            //console.log(result);

                            var nodemailer = require('nodemailer');
                            var smtpTransport = require('nodemailer-smtp-transport');
            
                            var mailAccountUser = 'testsmtptt@gmail.com'
                            var mailAccountPassword = 'testsmtppassword'
                            //var mailAccountPassword =process.env.GMAIL_PASS
            
                            var fromEmailAddress = 'testsmtptt@gmail.com'
                            var toEmailAddress = req.body.email
            
                            var transport = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: mailAccountUser,
                                    pass: mailAccountPassword
                                }
                            })
            
                            var mail = {
                                from: fromEmailAddress,
                                to: toEmailAddress,
                                subject: "Email Verification",
                                text: "Job Portal",
                                html: "Your OTP  is: <br><p><b>"+otp+"</b></p>"
                            }
            
                            transport.sendMail(mail, function(error, response){
                                if(error){
                                    console.log(error);
                                }else{
                                    console.log("Message sent: ");
                                    //console.log(response);
                                }
            
                                transport.close();
                            })
                            
                            callback(null, result);
                        }
                        //client.close();
                    })
        }       
    }) 
    
    
    })  
}


// Update Password
module.exports.updatePassword= function(req, res, callback){         
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                if(err){
                    throw err;
                }else{
                    var fieldName=hash;
                    const db=client.db(databaseName);
                    db.collection(collectionName).findOneAndUpdate({"email": req.body.email}, { "$set": { "password": fieldName}},
                    {
                        returnOriginal: false,
                        projection:{"password":0,"token":0,"otp":0}
                    },
                        function (err, memberData) { 
                            client.close();                           
                        if(err){
                            callback(err);
                        }else{              
                            
                            if(memberData){
                                callback(null, memberData);
                            }else{
                                callback(null,{msg: "Something Wrong. Try again leter"});
                            } 

                            
                        }
                    });
                }
            });            
        }
    });    
}


// Get Notices Value
module.exports.getNotices= function(req, res, callback){
    var user_id=sessionValue.user_id;  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("notice").find({status:1}).toArray(function (err, noticeData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(noticeData){ 
                        callback(null, noticeData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}


// Get Notice 
module.exports.getNotice= function(req, res, callback){
    var notice_id=req.query.notice_id; 
     //console.log(notice_id);
    // return;
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("notice").findOne({notice_id:parseInt(notice_id) , status:1}, function (err, noticeData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(noticeData){ 
                       // console.log(noticeData);
                        callback(null, noticeData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}


// Get Notices Value
module.exports.getlatestNotice= function(req, res, callback){
    var user_id=sessionValue.user_id;  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("notice").find({status:1}).limit(1).sort({$created_date:-1}).toArray( function (err, noticeData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(noticeData){ 
                        callback(null, noticeData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}


//get job list

module.exports.jobList = function(req, res, callback){ 
    console.log("pageno",req.query)
    // console.log("location param",req.query.loc);
    if(req.query.loc) {
        var loc = req.query.loc;
    }else{
        var loc = '';
    }
    if(req.query.jtype) {
        var jType = req.query.jtype;
    }else{
        var jType = '';
    }

    let x = new Date();
    x.setDate(x.getDate() - 10)
       
    // let exp =  x.setDate(x.getDate() - 7); 
    let expiaryDate = date.format(x, 'YYYY/MM/DD HH:mm:ss');
    const pageNo = req.query.page && Number(req.query.page) > 0 ? Number(req.query.page) : 1;
    const limitValue = 2;
    const skipValue = (pageNo - 1) * limitValue;
    console.log(expiaryDate);  
     if(Object.keys(req.query).length){
        connectDb(function(err, client){
            if(err){
                throw err;
            }else{
                const db=client.db(databaseName); // Enter Database Name
                db.collection("jobPostCollection").find().toArray (function(err,totalData) {
                    db.collection("jobPostCollection").find({ created_date: { $gt: expiaryDate },location :{ $regex:loc, $options: 'si' },jobType :{ $regex:jType, $options: 'si' } }).sort( { 'created_date': -1 }).skip(skipValue).limit(limitValue).toArray(function (err, jobPostData) {
                        
                        if(err){
                            callback(err);
                        }else{                   
                            //console.log(noticeData);
                            //return;
                            if(jobPostData){ 
                                sessionValue = req.session;
                                
                                for(let i = 0; i<=jobPostData.length-1;i++) {
                                    let eachJob = jobPostData[i]
                                    if(jobPostData[i].applications) {
                                        let applicationArray = jobPostData[i].applications
                                    
                                       
                                    for(let y = 0; y<=applicationArray.length-1;y++ ) {
                                        console.log("applicant id ....",applicationArray[y].applicant_id);
                                        if(applicationArray[y].applicant_id == req.session.member_id) {
                                            jobPostData[i].appliedCandidate = true;
                                        }
    
                                    }
                                    }
                                    
                                }
                                db.collection("jobPostCollection").find({location :{ $regex:loc, $options: 'si' },jobType :{ $regex:jType, $options: 'si' } }).toArray(function(err,totalValue) {
                                    if(err) {
                                        callback(err)
                                    }else{
                                        let locationArray =[]
                                        let locArray = [];
                                        for(let x = 0;x<=totalData.length-1;x++){
                                            locationArray.push(totalData[x].location)
                                        }
                                        let unique_location = [...new Set(locationArray)];
    
    
                                        console.log("8888888",unique_location);
                                        let numOfPage = totalValue.length / limitValue
                                        let pageno = Math.ceil(numOfPage)
                                        console.log(pageno);
                                        console.log(Array(pageno).fill("aa"))
                                        let myArray = Array(pageno).fill("aa")
                                        
                                        jobPostData["pageArray"] = myArray
                                        console.log("..............",jobPostData.pageArray);
                                        console.log("11111",unique_location);
                                        for(let j= 0;j<unique_location.length;j++) {
                                            if(unique_location[j] == loc) {
                                                locArray.push({"location":unique_location[j],checked:true})
                                            }else{
                                                locArray.push({"location":unique_location[j],checked:false})
                                            }
                                        }

                                        totalData["unique_locations"] = locArray
                                        //job type portion
                                        let jobTypeArr =[]
                                        let jTypeArr = [];
                                        for(let k = 0;k<=totalData.length-1;k++){
                                            jobTypeArr.push(totalData[k].jobType)
                                        }
                                        let unique_jobType = [...new Set(jobTypeArr)];
                                        for(let l= 0;l<unique_jobType.length;l++) {
                                            if(unique_jobType[l] == jType) {
                                                jTypeArr.push({"jobType":unique_jobType[l],checked:true})
                                            }else{
                                                jTypeArr.push({"jobType":unique_jobType[l],checked:false})
                                            }
                                        }
                                        totalData["unique_jobTypes"] = jTypeArr;

                                        console.log("job types...",totalData.unique_jobTypes)

                                        //console.log("888888880000",jobPostData)
                                        console.log(totalData.unique_locations)
                                        callback(null, jobPostData,totalData);
    
                                    }
                                })
    
    
                            }else{
                                callback(null);
                            }
                        }
                    }); 
                }) 
            }                
        })
    }else{
        connectDb(function(err, client){
            if(err){
                throw err;
            }else{
                const db=client.db(databaseName); // Enter Database Name
                    db.collection("jobPostCollection").find({ created_date: { $gt: expiaryDate }}).sort( { 'created_date': -1 }).skip(skipValue).limit(limitValue).toArray(function (err, jobPostData) {
                        
                        if(err){
                            callback(err);
                        }else{                   
                            //console.log(noticeData);
                            //return;
                            if(jobPostData){ 
                                sessionValue = req.session;
                                //for apply btn
                                for(let i = 0; i<=jobPostData.length-1;i++) {
                                    let eachJob = jobPostData[i]
                                    if(jobPostData[i].applications) {
                                        let applicationArray = jobPostData[i].applications
                                    
                                       
                                    for(let y = 0; y<=applicationArray.length-1;y++ ) {
                                        console.log("applicant id ....",applicationArray[y].applicant_id);
                                        if(applicationArray[y].applicant_id == req.session.member_id) {
                                            jobPostData[i].appliedCandidate = true;
                                        }
    
                                    }
                                    }
                                    
                                }

                                //for save btn

                                for(let i = 0; i<=jobPostData.length-1;i++) {
                                    let eachJob = jobPostData[i]
                                    if(jobPostData[i].saveJobs) {
                                        let saveJobArray = jobPostData[i].saveJobs
                                    
                                       
                                    for(let y = 0; y<=saveJobArray.length-1;y++ ) {
                                        console.log("applicant id ....",saveJobArray[y].applicant_id);
                                        if(saveJobArray[y].applicant_id == req.session.member_id) {
                                            jobPostData[i].jobSaved = true;
                                        }
    
                                    }
                                    }
                                    
                                }
                                db.collection("jobPostCollection").find({ created_date: { $gt: expiaryDate }}).toArray(function(err,totalValue) {
                                    if(err) {
                                        callback(err)
                                    }else{
                                        //location portion.........
                                        let locationArray =[]
                                        let locArray = [];
                                        for(let x = 0;x<=totalValue.length-1;x++){
                                            locationArray.push(totalValue[x].location)
                                        }
                                        let unique_location = [...new Set(locationArray)];
                                        for(let j= 0;j<unique_location.length;j++) {
                                            if(unique_location[j] == loc) {
                                                locArray.push({"location":unique_location[j],checked:true})
                                            }else{
                                                locArray.push({"location":unique_location[j],checked:false})
                                            }
                                        }
                                        totalValue["unique_locations"] = locArray;

                                        //page portion........
                                        let numOfPage = totalValue.length / limitValue
                                        let pageno = Math.ceil(numOfPage)
                                        console.log(pageno);
                                        console.log(Array(pageno).fill("aa"))
                                        let myArray = Array(pageno).fill("aa")
                                        
                                        jobPostData["pageArray"] = myArray
                                        console.log("..............",jobPostData.pageArray);

                                        //job Type portion                                        
                                        let jobTypeArr =[]
                                        let jTypeArr = [];
                                        for(let k = 0;k<=totalValue.length-1;k++){
                                            jobTypeArr.push(totalValue[k].jobType)
                                        }
                                        let unique_jobType = [...new Set(jobTypeArr)];
                                        for(let l= 0;l<unique_jobType.length;l++) {
                                            if(unique_jobType[l] == jType) {
                                                jTypeArr.push({"jobType":unique_jobType[l],checked:true})
                                            }else{
                                                jTypeArr.push({"jobType":unique_jobType[l],checked:false})
                                            }
                                        }
                                        totalValue["unique_jobTypes"] = jTypeArr;

                                        console.log("job types...",totalValue.unique_jobTypes)

                                        //console.log("888888880000",totalValue)
                                        //console.log(totalValue.unique_location)
                                        callback(null, jobPostData,totalValue);
    
                                    }
                                })
    
    
                            }else{
                                callback(null);
                            }
                        }
                    });
            }                
        });
  
     }
    //var user_id=1;
    
}

//get filtered job list
module.exports.filteredJobList = function(req, res, callback){ 
    console.log("pageno",req.query.page);
    console.log("location.....",req.query.loc);
    
    let x = new Date();
    x.setDate(x.getDate() - 10)
       
    // let exp =  x.setDate(x.getDate() - 7); 
    let expiaryDate = date.format(x, 'YYYY/MM/DD HH:mm:ss');
    const pageNo = req.query.page && Number(req.query.page) > 0 ? Number(req.query.page) : 1;
    const limitValue = 2;
    const skipValue = (pageNo - 1) * limitValue;
    console.log(expiaryDate);  
     
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
                db.collection("jobPostCollection").find({ created_date: { $gt: expiaryDate },location :{ $regex: req.query.loc, $options: 'si' }}).sort( { 'created_date': -1 }).skip(skipValue).limit(limitValue).toArray(function (err, jobPostData) {
                    
                    if(err){
                        callback(err);
                    }else{                   
                        //console.log(noticeData);
                        //return;
                        if(jobPostData){ 
                            sessionValue = req.session;
                            
                            for(let i = 0; i<=jobPostData.length-1;i++) {
                                let eachJob = jobPostData[i]
                                if(jobPostData[i].applications) {
                                    let applicationArray = jobPostData[i].applications
                                
                                   
                                for(let y = 0; y<=applicationArray.length-1;y++ ) {
                                    console.log("applicant id ....",applicationArray[y].applicant_id);
                                    if(applicationArray[y].applicant_id == req.session.member_id) {
                                        jobPostData[i].appliedCandidate = true;
                                    }

                                }
                                }
                                
                            }
                        db.collection("jobPostCollection").find().toArray(function(err,totalData) {
                                if(err) {
                                    callback(err)
                                }else{
                            db.collection("jobPostCollection").find({location :{ $regex: req.query.loc, $options: 'si' }}).toArray(function(err,totalValue) {
                                if(err) {
                                    callback(err)
                                }else{
                                    let locationArray =[]
                                    for(let x = 0;x<=totalData.length-1;x++){
                                        locationArray.push(totalData[x].location)
                                    }
                                    let unique_location = [...new Set(locationArray)];


                                    console.log("8888888",unique_location);
                                    let numOfPage = totalValue.length / limitValue
                                    let pageno = Math.ceil(numOfPage)
                                    console.log(pageno);
                                    console.log(Array(pageno).fill("aa"))
                                    let myArray = Array(pageno).fill("aa")
                                    
                                    jobPostData["pageArray"] = myArray
                                    console.log("..............",jobPostData.pageArray);
                                    totalData["unique_locations"] = unique_location
                                    
                                    callback(null, jobPostData,totalData);

                                }
                            })
                        }
                    })

                        }else{
                            callback(null);
                        }
                    }
                });
            
            
        }                
    });
}


module.exports.searchedJobs = function(req, res, callback){
    console.log("....../....")
    console.log(req.body.title)
    console.log(req.body.location)
    let searchedTitle = req.body.title;
    let searchedLocation = req.body.location;

    let x = new Date();
    x.setDate(x.getDate() - 10)
    let expiaryDate = date.format(x, 'YYYY/MM/DD HH:mm:ss');
    const pageNo = req.query.page && Number(req.query.page) > 0 ? Number(req.query.page) : 1;
    const limitValue = 1;
    const skipValue = (pageNo - 1) * limitValue;
    console.log(expiaryDate);  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
                db.collection("jobPostCollection").find({ created_date: { $gt: expiaryDate },title :{ $regex: searchedTitle, $options: 'si' },location :{ $regex: searchedLocation, $options: 'si' }}).sort( { 'created_date': -1 } ).skip(skipValue).limit(limitValue).toArray(function (err, jobPostData) {
                    
                    if(err){
                        callback(err);
                    }else{                   
                        //console.log(noticeData);
                        //return;
                        if(jobPostData){ 
                            let sessionValue = req.session;
                            for(let i = 0; i<=jobPostData.length-1;i++) {
                                let eachJob = jobPostData[i]
                                if(jobPostData[i].applications) {
                                    let applicationArray = jobPostData[i].applications
                                
                                   
                                for(let y = 0; y<=applicationArray.length-1;y++ ) {
                                    console.log("applicant id ....",applicationArray[y].applicant_id);
                                    if(applicationArray[y].applicant_id == req.session.member_id) {
                                        jobPostData[i].appliedCandidate = true;
                                    }

                                }
                                }
                                
                            }
                            db.collection("jobPostCollection").find({title :{ $regex: searchedTitle, $options: 'si' },location :{ $regex: searchedLocation, $options: 'si' }}).toArray(function(err,totalValue) {
                                if(err) {
                                    callback(err)
                                }else{
                                    let locationArray =[]
                                    for(let x = 0;x<=totalValue.length-1;x++){
                                        locationArray.push(totalValue[x].location)
                                    }
                                    let unique_location = [...new Set(locationArray)];
                                    totalValue["unique_locations"] = unique_location;


                                    console.log("8888888",totalValue.length/2)
                                    let numOfPage = totalValue.length / limitValue
                                    let pageno = Math.floor(numOfPage)
                                    console.log(pageno);
                                    console.log(Array(pageno).fill("aa"))
                                    let myArray = Array(pageno).fill("aa")
                                    
                                    jobPostData["pageArray"] = myArray
                                    console.log("..............",jobPostData.pageArray);
                                    sessionValue.title = searchedTitle;
                                    sessionValue.location = searchedLocation;
                                    callback(null, jobPostData,totalValue);

                                }
                            })                       
                        }else{
                            callback(null);
                        }
                    }
                });
            





            
        }                
    });
}
module.exports.filteredJobs = function(req, res, callback){
    console.log("....../....")
    //console.log(req.body.title)
    console.log(req.body.location)
    //let searchedTitle = req.body.title;
    let searchedLocation = req.body.location;
    console.log(searchedLocation)
    
    let x = new Date();
    x.setDate(x.getDate() - 10)
    let expiaryDate = date.format(x, 'YYYY/MM/DD HH:mm:ss');
    const pageNo = req.query.page && Number(req.query.page) > 0 ? Number(req.query.page) : 1;
    const limitValue = 1;
    const skipValue = (pageNo - 1) * limitValue;
    console.log(expiaryDate);  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
                db.collection("jobPostCollection").find({ created_date: { $gt: expiaryDate },location :{ $regex: searchedLocation, $options: 'si' }}).sort( { 'created_date': -1 } ).skip(skipValue).limit(limitValue).toArray(function (err, jobPostData) {
                    
                    if(err){
                        callback(err);
                    }else{                   
                        //console.log(noticeData);
                        //return;
                        if(jobPostData){ 
                            let sessionValue = req.session;
                            for(let i = 0; i<=jobPostData.length-1;i++) {
                                let eachJob = jobPostData[i]
                                if(jobPostData[i].applications) {
                                    let applicationArray = jobPostData[i].applications
                                
                                   
                                for(let y = 0; y<=applicationArray.length-1;y++ ) {
                                    console.log("applicant id ....",applicationArray[y].applicant_id);
                                    if(applicationArray[y].applicant_id == req.session.member_id) {
                                        jobPostData[i].appliedCandidate = true;
                                    }

                                }
                                }
                                
                            }
                            db.collection("jobPostCollection").find({location :{ $regex: searchedLocation, $options: 'si' }}).toArray(function(err,totalValue) {
                                if(err) {
                                    callback(err)
                                }else{
                                    
                                    console.log("8888888",totalValue.length/2)
                                    let numOfPage = totalValue.length / limitValue
                                    let pageno = Math.floor(numOfPage)
                                    console.log(pageno);
                                    console.log(Array(pageno).fill("aa"))
                                    let myArray = Array(pageno).fill("aa")
                                    
                                    jobPostData["pageArray"] = myArray
                                    console.log("..............",jobPostData.pageArray);
                                    //sessionValue.title = searchedTitle;
                                    sessionValue.location = searchedLocation;
                                    callback(null, jobPostData);

                                }
                            })                       
                        }else{
                            callback(null);
                        }
                    }
                });
            





            
        }                
    });
}

module.exports.searchedJobsGet = function(req, res, callback){

    console.log("....../....")
    console.log(req.query.location)   
    console.log(req.query.page)
    
    let searchedTitle = req.query.title;
    let searchedLocation = req.query.location;

    let x = new Date();
    x.setDate(x.getDate() - 10)
    let expiaryDate = date.format(x, 'YYYY/MM/DD HH:mm:ss');
    const pageNo = req.query.page && Number(req.query.page) > 0 ? Number(req.query.page) : 1;
    const limitValue = 1;
    const skipValue = (pageNo - 1) * limitValue;
    console.log(expiaryDate);  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
                db.collection("jobPostCollection").find({ created_date: { $gt: expiaryDate },title :{ $regex: searchedTitle, $options: 'si' },location :{ $regex: searchedLocation, $options: 'si' }}).sort( { 'created_date': -1 } ).skip(skipValue).limit(limitValue).toArray(function (err, jobPostData) {
                    
                    if(err){
                        callback(err);
                    }else{                   
                        //console.log(noticeData);
                        //return;
                        if(jobPostData){ 
                            let sessionValue = req.session;
                            for(let i = 0; i<=jobPostData.length-1;i++) {
                                let eachJob = jobPostData[i]
                                if(jobPostData[i].applications) {
                                    let applicationArray = jobPostData[i].applications
                                
                                   
                                for(let y = 0; y<=applicationArray.length-1;y++ ) {
                                    console.log("applicant id ....",applicationArray[y].applicant_id);
                                    if(applicationArray[y].applicant_id == req.session.member_id) {
                                        jobPostData[i].appliedCandidate = true;
                                    }

                                }
                                }
                                
                            }
                            db.collection("jobPostCollection").find({title :{ $regex: searchedTitle, $options: 'si' },location :{ $regex: searchedLocation, $options: 'si' }}).toArray(function(err,totalValue) {
                                if(err) {
                                    callback(err)
                                }else{
                                    
                                    console.log("8888888",totalValue.length/2)
                                    let numOfPage = totalValue.length / limitValue
                                    let pageno = Math.floor(numOfPage)
                                    console.log(pageno);
                                    console.log(Array(pageno).fill("aa"))
                                    let myArray = Array(pageno).fill("aa")
                                    
                                    jobPostData["pageArray"] = myArray
                                    console.log("..............",jobPostData.pageArray);
                                    
                                    callback(null, jobPostData);

                                }
                            })                      
                        }else{
                            callback(null);
                        }
                    }
                });
            





            
        }                
    });
}