const express=require('express');
var expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue

var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="manuscript"; // Define Collection Name

//Get App
const app= express.Router();
app.use(expressValidator())



// Get visite Count
module.exports.getDashboard= function(req, res, callback){
    //var user_id=sessionValue.user_id;  
    //var user_id=1;    
    var fieldName="visiteCount" 
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("viewCount").findOne({"visiteCount": fieldName}, function (err, dashboardData) {
                client.close();
                if(err){
                    callback(err);
                }else{   
                    //console.log(dashboardData);  
                    if(dashboardData){ 
                       // console.log();
                        callback(null, dashboardData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

// Get Member Count
module.exports.getMembers= function(req, res, callback){
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("member").find().toArray(function (err, memberData) {
                client.close();
                if(err){
                    callback(err);
                }else{   
                    //console.log(memberData);  
                    if(memberData){ 
                        //console.log(memberData);
                        callback(null, memberData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

// Get Member Count
module.exports.getJournals= function(req, res, callback){
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("journal").find().toArray(function (err, journalData) {
                client.close();
                if(err){
                    callback(err);
                }else{   
                    //console.log(journalData);  
                    if(journalData){ 
                        //console.log(journalData);
                        callback(null, journalData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}
