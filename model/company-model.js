const express=require('express');
var expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue

var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="companiesCollection"; // Define Collection Name

//Get App
const app= express.Router();
app.use(expressValidator())


//Job Category Deatils

//Add New
module.exports.addNewCompany = function(req, res, callback){
    req.collectionName=collectionName; // Enter Collection Name For Create Collection Index
    console.log(req.body.companyName +"check..")
    collectionIndex.checkIndex(req, res, function(result){
        let now = new Date();
        var item={
            company_id:result,
            user_id: req.session.user_id,
            companyName: req.body.companyName,
            companyDetails:req.body.companyDetails, 
            companyType: req.body.companyType,   
            companyAddress: req.body.companyAddress,         
            companyEmail: req.body.companyEmail,         
            phone: req.body.phone,         
            image:req.file.filename,
            companyAuthenticated:false,
            rejectionCause : "pending..",         
            created_ip_address:req.connection.remoteAddress,
            modified_ip_address:req.connection.remoteAddress,
            created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            status:parseInt(req.body.status)
        }
        connectDb(function(err, client){
            if(err){
                throw err;
            }else{
                const db=client.db(databaseName); // Enter Database Name
                db.collection(collectionName).insertOne(item, function(err,result){
                    client.close();
                    if(err){
                        throw err;
                    }else{
                        console.log(result);
                        callback(null, result);
                    }
                    
                })
            }                
        });
                                
    })  
}


//Get 
module.exports.rejectedList = function(req, res, callback){
    var user_id = sessionValue.user_id; 
    console.log("3333333333333",req.session.user_id) 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).find({"user_id" : req.session.user_id,"companyAuthenticated":false}).toArray(function (err, rejectedCompanyData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(rejectedCompanyData){ 
                        callback(null, rejectedCompanyData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

//Edit Category
module.exports.editCategory= function(req, res, callback){
    let now = new Date();
    var category_id = req.body.category_id;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({category_id:parseInt(category_id)}, function (err, jobCategoryData) {
                if(err){
                    callback(err);
                }else{
                  
                    var categoryName = req.body.categoryName;
                    var categoryDetails = req.body.categoryDetails;
                    
                    var modified_ip_address=req.connection.remoteAddress;
                    var modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');        
                    
                    db.collection(collectionName).findOneAndUpdate({category_id:parseInt(category_id)}, { "$set": {
                        "categoryName" : categoryName,
                        "categoryDetails" : categoryDetails, 
                        "modified_ip_address": modified_ip_address, 
                        "modified_date": modified_date
                    }},{ 
                        returnOriginal: false
                    },function (err, categoryData)
                    {
                        client.close();
                        if(err){
                            callback(err);
                        }else{       
                            callback(null, categoryData);   
                        }
                    });
                }
            });
            
        }                
    });
}


//Edit fetch category
module.exports.editGetCategory= function(req, res, callback){
    var category_id = req.query.category_id; 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({category_id:parseInt(category_id)}, function (err, categoryData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(categoryData){ 
                        console.log(categoryData);
                        callback(null, categoryData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

//Delete Category
module.exports.deleteCategory = function(req, res, callback){
    var category_id = req.query.category_id; 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOneAndDelete({category_id:parseInt(category_id)}, function (err, categoryData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(categoryData){ 
                        console.log(categoryData);
                        callback(null, categoryData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}