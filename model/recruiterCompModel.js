const express=require('express');
var expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue

var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="jobPostCollection"; // Define Collection Name

//Get App
const app= express.Router();
app.use(expressValidator())


//Job Post Deatils
module.exports.addNewJob = function(req, res, callback){
    console.log("job post model...");
    req.collectionName=collectionName; // Enter Collection Name For Create Collection Index   
    collectionIndex.checkIndex(req, res, function(result){
        let now = new Date();
        var item={
            jobPost_id:result,
            user_id:req.session.user_id,
            companyName: req.body.companyName,
            title:req.body.title,
            jobCategory:req.body.jobCategory,
            jobType:req.body.jobType,
            location:req.body.location,      
            description:req.body.description,
            created_ip_address:req.connection.remoteAddress,
            modified_ip_address:req.connection.remoteAddress,
            created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            status:parseInt(req.body.status)
        }
        connectDb(function(err, client){
            if(err){
                throw err;
            }else{
                const db=client.db(databaseName); // Enter Database Name
                db.collection(collectionName).insertOne(item, function(err,result){
                    client.close();
                    if(err){
                        throw err;
                    }else{
                        console.log(result);
                        callback(null, result);
                    }
                    
                })
            }                
        });
                                
    })  
}

module.exports.getJobPosted = function(req, res, callback){
    let searchElm = req.query.search;
    
    var user_id = sessionValue.user_id;  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            if(searchElm) {
                db.collection(collectionName).find({"user_id": parseInt(req.session.user_id)}).toArray(function(err,allData) {
                    db.collection(collectionName).find({"companyName":searchElm}).toArray(function (err, jobPostData) {
                        client.close();
                        if(err){
                            callback(err);
                        }else{                   
                            //console.log(noticeData);
                            //return;
                            if(jobPostData){ 
                                
                                callback(null, jobPostData,allData);                        
                            }else{
                                callback(null);
                            }
                        }
                    });
                })


                
            }else{
                db.collection(collectionName).find({"user_id":parseInt(req.session.user_id)}).toArray(function (err, jobPostData) {
                    client.close();
                    if(err){
                        callback(err);
                    }else{                   
                        //console.log(noticeData);
                        //return;
                        if(jobPostData){ 
                            callback(null, jobPostData,jobPostData);                        
                        }else{
                            callback(null);
                        }
                    }
                });
            }





            
        }                
    });
}
//get the company in dashboard
module.exports.getCompany = function(req, res, callback){
    console.log("req.query.compId...",req.query.compId);
    console.log("req.query.compId...",req.query.compName);
   

    var user_id = sessionValue.user_id;  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("companiesCollection").findOne({"user_id":req.session.user_id,"company_id":parseInt(req.query.compId)},function (err, companyData) {
            if(err) {
                callback(err);
            }else{
                let sessionValue = req.session;
                sessionValue.company_id = req.query.compId;
                sessionValue.companyName = req.query.compName;

                callback(null,companyData);
            }
            })
        }                
    });
}


module.exports.getCompanies = function(req, res, callback){
    var user_id = sessionValue.user_id;  
    let collectionName = "companiesCollection"
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).find({"user_id": parseInt(req.session.user_id),"companyAuthenticated": true}).toArray(function(err,comapanies) {
                if(err){
                    callback(err);
                }else{
                    callback(null,comapanies);
                }

            })

        }                
    });
}
