const express=require('express');
var expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue

var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="jobPostCollection"; // Define Collection Name

//Get App
const app= express.Router();
app.use(expressValidator())


//Job Category Deatils




//Get all company application
module.exports.getAllApplications = function(req, res, callback){
    var user_id = sessionValue.user_id;  
    console.log("......."+ req.session.user_id)
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).find({"user_id":parseInt(req.session.user_id) }).toArray(function (err, jobApplicationData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(jobApplicationData){ 
                        
                        callback(null, jobApplicationData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

//Get specific company application
module.exports.getCompanyApplications = function(req, res, callback){
    //var user_id = sessionValue.user_id;  
    console.log("......."+ req.session.user_id)
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).find({"user_id":parseInt(req.session.user_id),"company_id":req.session.company_id}).toArray(function (err, jobApplicationData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(jobApplicationData){ 
                        
                        callback(null, jobApplicationData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}


module.exports.viewApplicant = function(req, res, callback){
    console.log(req.query.applicant_id)
    let applicant_id = req.query.applicant_id
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection("member1").findOne({"member_id":parseInt(applicant_id) },{projection:{"password":0,"token":0}}, function (err, applicantData) {
                
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    //console.log(noticeData);
                    //return;
                    if(applicantData){ 
                        console.log("applicantData",applicantData);
                        applicantData["company_name"] = req.query.company_name
                        applicantData["jobPost_id"] = req.query.job_id
                        callback(null, applicantData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}




//Edit Category
module.exports.editCategory= function(req, res, callback){
    let now = new Date();
    var category_id = req.body.category_id;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({category_id:parseInt(category_id)}, function (err, jobCategoryData) {
                if(err){
                    callback(err);
                }else{
                  
                    var categoryName = req.body.categoryName;
                    var categoryDetails = req.body.categoryDetails;
                    
                    var modified_ip_address=req.connection.remoteAddress;
                    var modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');        
                    
                    db.collection(collectionName).findOneAndUpdate({category_id:parseInt(category_id)}, { "$set": {
                        "categoryName" : categoryName,
                        "categoryDetails" : categoryDetails, 
                        "modified_ip_address": modified_ip_address, 
                        "modified_date": modified_date
                    }},{ 
                        returnOriginal: false
                    },function (err, categoryData)
                    {
                        client.close();
                        if(err){
                            callback(err);
                        }else{       
                            callback(null, categoryData);   
                        }
                    });
                }
            });
            
        }                
    });
}


//Edit fetch category
module.exports.editGetCategory= function(req, res, callback){
    var category_id = req.query.category_id; 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({category_id:parseInt(category_id)}, function (err, categoryData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(categoryData){ 
                        console.log(categoryData);
                        callback(null, categoryData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

//Delete Category
module.exports.deleteCategory = function(req, res, callback){
    var category_id = req.query.category_id; 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOneAndDelete({category_id:parseInt(category_id)}, function (err, categoryData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(categoryData){ 
                        console.log(categoryData);
                        callback(null, categoryData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}