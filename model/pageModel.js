const express=require('express');
var expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue

var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="page"; // Define Collection Name

//Get App
const app= express.Router();
app.use(expressValidator())



// Add Journal
module.exports.add= function(req, res, callback){
    req.collectionName=collectionName; // Enter Collection Name For Create Collection Index
    collectionIndex.checkIndex(req, res, function(result){
        let now = new Date();
        var item={
            title:req.body.title,
            slug:req.body.slug,
            page_id:result,
            description:req.body.description,
            user_id:req.session.user_id,
            created_ip_address:req.connection.remoteAddress,
            modified_ip_address:req.connection.remoteAddress,
            created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
            status:req.body.status
        }
        connectDb(function(err, client){
            if(err){
                throw err;
            }else{
                const db=client.db(databaseName); // Enter Database Name
                db.collection(collectionName).insertOne(item, function(err,result){
                    client.close();
                    if(err){
                        throw err;
                    }else{
                        callback(null, result);
                    }
                    
                })
            }                
        });
                                
    })  
}


// Get Jouranls Value
module.exports.getPages= function(req, res, callback){
    var user_id=sessionValue.user_id;  
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).find({user_id:user_id}).toArray(function (err, pageData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(pageData){ 
                        callback(null, pageData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

// Get Jouranl Value
module.exports.getPage= function(req, res, callback){
    var jrnl_id=req.query.page_id; 
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({page_id:parseInt(jrnl_id)}, function (err, pageData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(pageData){ 

                        callback(null, pageData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}


// Delete Jouranl Value
module.exports.deleteJournal= function(req, res, callback){
    var jrnl_id=req.query.page_id;  
    
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            //console.log(jrnl_id);
            db.collection(collectionName).findOneAndDelete({page_id:parseInt(jrnl_id)},function (err, studentData) {
                client.close();
                if(err){
                    callback(err);
                }else{     
                    //console.log(studentData);     
                    callback(null, studentData);   
                }
            });
        }                
    });
}

// Edit Jouranl Value
module.exports.edit= function(req, res, callback){
    let now = new Date();
    var page_id=req.body.page_id;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({page_id:parseInt(page_id)}, function (err, pageData) {
                if(err){
                    callback(err);
                }else{
                   
                    var title=req.body.title;
                    var slug=req.body.slug;
                    var description=req.body.description;  
                    var status=req.body.status;           
                    var modified_ip_address=req.connection.remoteAddress;
                    var modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');        
                    
                    db.collection(collectionName).findOneAndUpdate({page_id:parseInt(page_id)}, { "$set": { "title": title, "slug": slug, "description": description, "status": status,"modified_ip_address": modified_ip_address, "modified_date": modified_date}},{ returnOriginal: false},function (err, studentData)
                    {
                        client.close();

                        if(err){
                            callback(err);
                        }else{       
                            callback(null, studentData);   
                        }
                    });
                }
            });
            
        }                
    });
}



// Status Jouranl Value
module.exports.status= function(req, res, callback){
    let now = new Date();
    var page_id=req.query.page_id;  
    
    //var user_id=1;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({page_id:parseInt(page_id)}, function (err, pageData) {
                if(err){
                    callback(err);
                }else{
                    if(pageData){
                        var status=1-pageData.status;           
                        var modified_ip_address=req.connection.remoteAddress;
                        var modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');        

                        db.collection(collectionName).findOneAndUpdate({page_id:parseInt(page_id)},{ "$set": { "status": status,"modified_ip_address": modified_ip_address, "modified_date": modified_date}},function (err, studentData) {
                            client.close();
                            if(err){
                                callback(err);
                            }else{         
                                callback(null, studentData);   
                            }
                        });
                    }else{
                        callback(pageData);
                    }
                    
                }
            });


            
        }                
    });
}
