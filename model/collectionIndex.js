const connectDb= require('../config/database'); // Get Database
const configRoute=require('../config/route'); // Get config rotue
var databaseName=configRoute.databaseName;


module.exports.checkIndex= function(req, res ,cb){
    connectDb(function(err, client){
        if(err){
            console.log(err);
        }else{
            var collectionName=req.collectionName;            
            const db=client.db(databaseName);
            db.collection("collectionIndex").findOneAndUpdate({"collectionName": collectionName}, {$inc: { last_id: 1}},
                function (err, friendData) { 
                    //client.close();                           
                if(err){
                    cb(err);
                }else{              
                    if(friendData.value){
                        cb(friendData.value.last_id+1)
                    }else{
                        var item={
                            collectionName:collectionName,
                            last_id:1
                            }
                        const db=client.db(databaseName);
                        db.collection("collectionIndex").insertOne(item, function(err,result){
                            client.close();
                            if(err){
                                cb(err);
                            }else{
                                cb(result.ops[0].last_id);
                            }
                        })
                    }
                }
                client.close(); 
            });
        }                
    })
}