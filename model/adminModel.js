const express=require('express');
var expressValidator  = require('express-validator');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const connectDb= require('../config/database');
const collectionIndex=require('./collectionIndex.js');
let date = require('date-and-time');
const configRoute=require('../config/route'); // Get config rotue
const cryptoRandomString = require('crypto-random-string');


var databaseName=configRoute.databaseName;// Store Database Name
var collectionName="admindata"; // Define Collection Name

//Get App
const app= express.Router();
app.use(expressValidator())



// Admin Registration
module.exports.registration= function(req, res, callback){
    let token = cryptoRandomString({length: 25, type: 'hex'})
    console.log(token);
    
    // console.log(urlToMail + " urlToMail");
    
    let urlToMail = `http://localhost:5000/admin/registration/${token}`;

    //req.collectionName="mailRegistration"; // Enter Collection Name For Create Collection Index
    req.collectionName=collectionName;
    collectionIndex.checkIndex(req, res, function(result){
        bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            let now = new Date();
            var item={
                user_id:result,       
                email:req.body.email,
                password:hash,
                token:token,
                user_role:1,
                emailVerified : "false",
                //image: documentImage,
                created_ip_address:req.connection.remoteAddress,
                modified_ip_address:req.connection.remoteAddress,
                created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
                modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
            }
            
            
            const db=client.db(databaseName); // Enter Database Name
                    db.collection(collectionName).insertOne(item, function(err,result){
                        client.close();
                        if(err){
                            throw err;
                        }else{
                            //console.log(result);

                            var nodemailer = require('nodemailer');
                            var smtpTransport = require('nodemailer-smtp-transport');
            
                            var mailAccountUser = 'testsmtptt@gmail.com'
                            var mailAccountPassword = 'testsmtppassword'
                            //var mailAccountPassword =process.env.GMAIL_PASS
            
                            var fromEmailAddress = 'testsmtptt@gmail.com'
                            var toEmailAddress = req.body.email
            
                            var transport = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: mailAccountUser,
                                    pass: mailAccountPassword
                                }
                            })
            
                            let mail = {
                                from: fromEmailAddress,
                                to: toEmailAddress,
                                subject: "Email Verify",
                                text: "Job Portal",
                                html: `Please click the link to verify the email:<a href = "${urlToMail}">login link click here...</a>`
                            }
            
                            transport.sendMail(mail, function(error, response){
                                if(error){
                                    console.log(error);
                                }else{
                                    console.log("Message sent: ");
                                    //console.log(response);
                                }
            
                                transport.close();
                            })
                            console.log(result.ops[0].email)
                            callback(null, result);
                        }
                        //client.close();
                    })
        }       
    }) 
    });           
    })  
}

// Admin Registration
module.exports.old_registration= function(req, res, callback){
    req.collectionName=collectionName; // Enter Collection Name For Create Collection Index
    collectionIndex.checkIndex(req, res, function(result){
        bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
            if(err){
                throw err;
            }else{
                let now = new Date();
                var item={
                    first_name:req.body.first_name,
                    last_name:req.body.last_name,
                    user_id:result,
                    email:req.body.email,
                    phone:req.body.phone,
                    password:hash,
                    username:req.body.username,
                    user_role:1,
                    image:req.file.filename,
                    created_ip_address:req.connection.remoteAddress,
                    modified_ip_address:req.connection.remoteAddress,
                    created_date:date.format(now, 'YYYY/MM/DD HH:mm:ss'),
                    modified_date:date.format(now, 'YYYY/MM/DD HH:mm:ss')
                }
                connectDb(function(err, client){
                    if(err){
                        throw err;
                    }else{
                        const db=client.db(databaseName); // Enter Database Name
                        db.collection(collectionName).insertOne(item, function(err,result){
                            client.close();
                            if(err){
                                throw err;
                            }else{
                                callback(null, result);
                            }
                            
                        })
                    }                
                });
            }
        });                    
    })  
}

//Admin Profile
module.exports.profile= function(req, res, callback){
    sessionValue=req.session;
    var user_id=sessionValue.user_id;  
    //var user_id=2;       
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({user_id:user_id}, function (err, userData) {
                if(err){
                    callback(err);
                }else{                   
                    if(userData){  
                        let now = new Date();

                        if(req.file){
                            
                            var image=req.file.filename;
                        }else{
                            var image=userData.image;
                        }
                        
                        var first_name=req.body.first_name;
                        var last_name=req.body.last_name;
                        var email=req.body.email;
                        var phone=req.body.phone;
                        var username=req.body.username;                            
                        var modified_ip_address=req.connection.remoteAddress;
                        var modified_date=date.format(now, 'YYYY/MM/DD HH:mm:ss');

                        db.collection(collectionName).findOneAndUpdate({user_id:user_id}, { "$set": { "first_name": first_name, "last_name": last_name, "email": email, "phone": phone, "username": username, "phone": phone, "image": image, "modified_ip_address": modified_ip_address, "modified_date": modified_date}},
                        { 
                            returnOriginal: false,
                            projection:{"password":0,"token":0,"otp":0}
                        }, function (err, updatedData){
                            client.close();
                            if(err){
                                callback(err);
                            }else{
                                
                                sessionValue=req.session;
                                sessionValue.image=updatedData.value.image; // Update session image
                                sessionValue.first_name=updatedData.first_name; // Update session First Name
                                sessionValue.last_name=updatedData.last_name; // Update session Last Name
                                
                                callback(null, updatedData);
                            }
                        });
                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}


// Get Admin Profile Value
module.exports.getProfile= function(req, res, callback){
    
    var user_id=sessionValue.user_id;  
    //var user_id=2;
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({user_id:user_id},{projection:{"password":0,"token":0,"otp":0}}, function (err, userData) {
                client.close();
                if(err){
                    callback(err);
                }else{                   
                    if(userData){ 
                        let sessionValue=req.session;
                        sessionValue.first_name = userData.first_name;
                        sessionValue.last_name = userData.last_name;
                        callback(null, userData);                        
                    }else{
                        callback(null);
                    }
                }
            });
        }                
    });
}

module.exports.login= function(req, res, callback){
    
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({ email: req.body.email }, function(err,result){
                
                client.close();     
                if(err){
                    throw err;
                }else{
                    if(result){
                        bcrypt.compare(req.body.password, result.password, function(err, isMatch) {
                            if(err){
                                throw err;
                            }else{
                                if(isMatch){
                                    console.log(".........."+ result.user_role);                                                                        
                                    sessionValue=req.session;
                                    sessionValue.user_id=result.user_id;
                                    sessionValue.first_name=result.first_name;
                                    sessionValue.last_name=result.last_name;
                                    sessionValue.email=result.email;
                                    sessionValue.phone=result.phone;
                                    sessionValue.username=result.username;
                                    sessionValue.user_role=result.user_role;
                                    sessionValue.image=result.image;

                                    callback(null, isMatch);
                                }else{
                                    
                                    callback(null,{msg: "password not match"});
                                }                                    
                            }
                        }); 
                    }else{
                       callback(null, {msg: "Incorrect Email"} );
                    }                                                                     
                }
            
            });
        }            
    });
}


// Admin Login
module.exports.old_login= function(req, res, callback){
    console.log(req.body.password);
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name
            db.collection(collectionName).findOne({ email: req.body.email }, function(err,result){
                console.log("in")
                client.close();     
                if(err){
                    throw err;
                }else{
                    if(result){
                        bcrypt.compare(req.body.password, result.password, function(err, isMatch) {
                            if(err){
                                throw err;
                            }else{
                                if(isMatch){
                                    console.log("matched");
                                    sessionValue=req.session;
                                    sessionValue.user_id=result.user_id;
                                    sessionValue.first_name=result.first_name;
                                    sessionValue.last_name=result.last_name;
                                    sessionValue.email=result.email;
                                    sessionValue.phone=result.phone;
                                    sessionValue.username=result.username;
                                    sessionValue.user_role=result.user_role;
                                    sessionValue.image=result.image;
                                    callback(null, isMatch);
                                }else{
                                    console.log("password not match")
                                    callback(null,{msg: "password not match"});
                                }                                    
                            }
                        }); 
                    }else{
                       callback(null, {msg: "Incorrect Email"} );
                    }                                                                     
                }
            
            });
        }            
    });
}

//Admin Forgot password
module.exports.forgotPassword= function(req, res, callback){

    var rn = require('random-number');
    var options = {
      min:  100000
    , max:  900000
    , integer: true
    }
    var otp=rn(options);

    req.collectionName="adminForgotPassword"; // Enter Collection Name For Create Collection Index
    collectionIndex.checkIndex(req, res, function(result){
        
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            let now = new Date();
            
            
            
            const db=client.db(databaseName); // Enter Database Name
                    db.collection("adminForgotPassword").findOneAndUpdate({
                        "email" : req.body.email
                    },{
                        $set : {
                            "otp":otp
                        }
                    },
                    {
                        upsert:true
                    },function(err,result){
                        client.close();
                        if(err){
                            throw err;
                        }else{
                            //console.log(result);

                            var nodemailer = require('nodemailer');
                            var smtpTransport = require('nodemailer-smtp-transport');
            
                            var mailAccountUser = 'testsmtptt@gmail.com'
                            var mailAccountPassword = 'testsmtppassword'
                            //var mailAccountPassword =process.env.GMAIL_PASS
            
                            var fromEmailAddress = 'testsmtptt@gmail.com'
                            var toEmailAddress = req.body.email
            
                            var transport = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: mailAccountUser,
                                    pass: mailAccountPassword
                                }
                            })
            
                            var mail = {
                                from: fromEmailAddress,
                                to: toEmailAddress,
                                subject: "Change Password",
                                text: "Job Portal",
                                html: "Your OTP  is: <br><p><b>"+otp+"</b></p>"
                            }
            
                            transport.sendMail(mail, function(error, response){
                                if(error){
                                    console.log(error);
                                }else{
                                    console.log("Message sent: ");
                                    //console.log(response);
                                }
            
                                transport.close();
                            })
                            
                            callback(null, result);
                        }
                        //client.close();



                    }
                        
                    )
        }       
    })            
    })  
}

// Update Password
module.exports.updatePassword= function(req, res, callback){         
    connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                if(err){
                    throw err;
                }else{
                    var fieldName=hash;
                    const db=client.db(databaseName);
                    db.collection(collectionName).findOneAndUpdate({"email": req.body.email}, { "$set": { "password": fieldName}},
                        function (err, memberData) { 
                            client.close();                           
                        if(err){
                            callback(err);
                        }else{              
                            
                            if(memberData){
                                callback(null, memberData);
                            }else{
                                callback(null,{msg: "Something Wrong. Try again leter"});
                            } 

                            
                        }
                    });
                }
            });            
        }
    });    
}

