const express=require('express');
var expbs  = require('express-handlebars');
const path=require('path');
const cors = require('cors');
var expressSession  = require('express-session');


// Init App
const app= express();


// app.use(cors());

// app.options('*', cors());

//socket connection
// let http = require("http").Server(app);
// let io = require("socket.io")(http)
// io.set('origins', 'localhost:5000');

// const io = require("socket.io")(http, {
//     handlePreflightRequest: (req, res) => {
//         const headers = {
//             "Access-Control-Allow-Headers": "Content-Type, Authorization",
//             "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
//             "Access-Control-Allow-Credentials": true
//         };
//         res.writeHead(200, headers);
//         res.end();
//     }
// });
// Set css static Path
app.use(express.static(path.join(__dirname, 'public')));

// Define Express Session
app.use(expressSession({        
    secret: 'secretkey',
    saveUninitialized: true,
    resave: true
}));

// Create custome helpers
const hbs=expbs.create({
    //defaultLayout: 'admin', 
    layoutsDir:path.join(__dirname,'views/welcome'),
    partialsDir:path.join(__dirname,'views/includes'),
    //partialsDir:path.join(__dirname,'views/includes/frontend'),
    helpers:{
        equals: function(operand_1, operator, operand_2, options) {
            var operators = {
             'eq': function(l,r) { return l == r; },
             'noteq': function(l,r) { return l != r; },
             'gt': function(l,r) { return Number(l) > Number(r); },
             'gte': function(l,r) { return Number(l) >= Number(r); },
             'lt': function(l,r) { return Number(l) < Number(r); },
             'lte': function(l,r) { return Number(l) <= Number(r); },
             'or': function(l,r) { return l || r; },
             'and': function(l,r) { return l && r; },
             '%': function(l,r) { return (l % r) === 0; }
            }
            , result = operators[operator](operand_1,operand_2);
          
            if (result) return options.fn(this);
            else  return options.inverse(this);
        },
        readmore: function(str) {
            if (str.length > 100)
              return str.substring(0,100) + '...';
            return str;
          },
        times:function(n, block) {
            var accum = '';
            for(var i = 0; i < n; ++i)
                accum += block.fn(i);
            return accum;
        },
        math: function(lvalue, operator, rvalue) {lvalue = parseFloat(lvalue);
            rvalue = parseFloat(rvalue);
            return {
                "+": lvalue + rvalue,
                "-": lvalue - rvalue,
                "*": lvalue * rvalue,
                "/": lvalue / rvalue,
                "%": lvalue % rvalue
            }[operator];
        },
        add: function(a, b) {return Number(a) + Number(b)}
    }    
})

// Init Express Handlebars
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');


//start server
app.listen(5000, function(){
    console.log("the server is running 5000");
    // io.on("connection", function(socket) {
    //     console.log("Connected!");
    //     socket.on("messageSent",function(message){
    //          console.log("....////......")
    //     });
    //     })
    
})

// var io = require('socket.io').listen(app.listen(5000));

// io.sockets.on('connection', function (socket) {
//     console.log("user" + socket.id);
//     socket.on("messageSent",function(message){

//         console.log("....////......")
  
//     })
//   })


//   io.on("connection", function(socket) {
//     console.log("Connected!");
//     socket.on("messageSent",function(message){
//          console.log("....////......")
//     });
//     })

// Backend Routes
const adminRoute=require('./routes/adminRoutes'); // Get Admin routes
app.use('/admin', adminRoute);

//Recruiter Routing
const recruiterRoute = require('./routes/recruiterRoutes'); // Get Admin routes
app.use('/recruiter', recruiterRoute);

// const journalRoute=require('./routes/journalRoutes'); // Get Journal routes
// app.use('/admin/journal', journalRoute);
const pageRoute=require('./routes/pageRoutes'); // Get Journal routes
app.use('/admin/page', pageRoute);

const seekersRoute=require('./routes/seekerRoutes'); // Get Journal routes
app.use('/admin/users', seekersRoute);
// const listManuscriptRoute=require('./routes/listManuscriptRoutes'); // Get List Manuscript routes
// app.use('/admin/manuscript', listManuscriptRoute);

// const listMember=require('./routes/listMember'); // Get List Manuscript routes
// app.use('/admin/member', listMember);

// const dashboardRoute=require('./routes/dashboardRoute'); // Get Dashboard routes
// app.use('/admin/dashboard', dashboardRoute);

//recruiter dashboard
const dashboardRoute=require('./routes/dashboardRoute'); // Get Dashboard routes
app.use('/recruiter/dashboard', dashboardRoute);

// const noticeRoutes=require('./routes/noticeRoutes'); // Get Notice routes
// app.use('/admin/notice', noticeRoutes);

// Frontend Routes
const homeRoute=require('./routes/homeRoutes'); // Get Home routes
app.use('/', homeRoute);


// const jobPostRoute=require('./routes/jobPostRoutes'); // Get JobPost routes
// app.use('/jobposts', jobPostRoute);

const jobPostRoute=require('./routes/jobPostRoutes'); // Get JobPost routes
app.use('/recruiter/jobposts', jobPostRoute);

const jobCategoryRoute = require('./routes/job-category-routes'); // Get JobPost routes
app.use('/recruiter/job-category', jobCategoryRoute);

//company routing
const companyRoute = require('./routes/company-routes'); // Get company routes
app.use('/recruiter/company', companyRoute);


//job application routing
// const applicationsRoute = require('./routes/applications-route'); // Get company routes
// app.use('/admin/job-applications', applicationsRoute);

//recruiter job application routing
const applicationsRoute = require('./routes/applications-route'); // Get company routes
app.use('/recruiter/job-applications', applicationsRoute);

//recruiter routes company specific..........

//comany open Dash
const recruiterCompDash = require('./routes/recruiter-comp-route'); // Get company routes
app.use('/recruiter/company', recruiterCompDash);




//Admin companies requests
const requestedCompanyRoute = require('./routes/requested-company-routes'); 
app.use('/admin/requested-companies', requestedCompanyRoute);

// const eJournalRoute=require('./routes/eJournalRoutes'); // Get Ejournal routes
// app.use('/journal', eJournalRoute);

// const manuscriptRoute=require('./routes/manuscriptRoutes'); // Get Manuscript routes
// app.use('/manuscript', manuscriptRoute);

//start server


app.use(function(req, res, next) {
    
    res.status(404).send('404 Not Found...');
});

