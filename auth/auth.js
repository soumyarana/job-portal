const express=require('express');
var bodyParser = require('body-parser');
var expressValidator  = require('express-validator');
var expressSession  = require('express-session');
var multer = require('multer');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
const validate = require('../utility/homeValidation.js');


const configRoute=require('../config/route'); // Get config rotue
var baseUrl=configRoute.baseUrl; // Get Base Url
// Use app
const app= express.Router();    //Get App
app.use(expressValidator())     //Define Express Validator
app.use(bodyParser.json());     // Define Bodyparser   
app.use(expressSession({        // Define Express Session
    secret: 'secretkey',
    saveUninitialized: true,
    resave: true
}));

// Include models and controllers
const Model = require('../model/recruiterModel');     // include model
const recruiterControllers = require('../controllers/recruiters');  // include controller
const homeControllers = require('../controllers/homes');  // include controller


// Auth portion


// Authentication Checke
module.exports.authentic =function(req, res, next){
    sessionValue=req.session;
    if(sessionValue.user_role==1){
        return next();
    }else{
        res.redirect('/recruiter/login');
    }    
}

// Login Checke
module.exports.checkLogin =function(req, res, next){
    sessionValue=req.session;
    if(sessionValue.user_role){
        res.redirect('/recruiter/profile');
    }else{        
        return next();
    }    
}
//Email verification check
// Login Checke
module.exports.emailVerified =function(req, res, next){
    req.session.emailNotVerified = false;
    const configRoute=require('../config/route'); // Get config rotue
    const connectDb= require('../config/database');
    let databaseName=configRoute.databaseName;// Store Database Name
    validate.login(req, res, function (errors) {
        if (errors) {
            console.log(errors);
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/login');
        } else {
            req.session.success = true;
            
        //check email verified or not
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name

            db.collection("member1").findOne({ "email": req.body.email }, function(err,result){
                client.close();
                
                if(err){
                    console.log("errror part")
                    req.session.errors = "Email is not registered"
                    return;
                }else{
                    if(result){
                        console.log(result.email + "Email address");
                        var loginEmail = result.email
                        if(result.emailVerified == "true"){
                            console.log("email verified..")
                            return next();
                        }else{
                            console.log("email is not verified..")
                            
                            req.session.success = false;
                            req.session.emailNotVerified = true;
                            req.session.loginEmail = loginEmail;    
                            res.redirect('/login');

                        }
                                               
                        // errors.push(error);                     
                    }else{
                        console.log("email is not registered..")
                    errormsg = []
                    errormsg.push({
                            msg: "Email is not registered.. "
                    });
                    req.session.errors = errormsg ;
                    req.session.success = false;
                    req.session.emailNotVerified = false;    
                    res.redirect('/login');
                    }
                    
                    // Here need  to be next part of action - not outside find callback!

                }


                
            });
         }                    
        }) 
    } 
})
       
}



//recruiter email verification check
module.exports.emailVerifiedrecruiter =function(req, res, next){
    req.session.emailNotVerified = false;
    const configRoute=require('../config/route'); // Get config rotue
    const connectDb= require('../config/database');
    let databaseName=configRoute.databaseName;// Store Database Name
    validate.login(req, res, function (errors) {
        if (errors) {
            console.log(errors);
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/recruiter/login');
        } else {
            req.session.success = true;
            
        //check email verified or not
        connectDb(function(err, client){
        if(err){
            throw err;
        }else{
            const db=client.db(databaseName); // Enter Database Name

            db.collection("userdata").findOne({ "email": req.body.email }, function(err,result){
                client.close();
                console.log("result", result)
                if(err){
                    console.log("errror part")
                    req.session.errors = "Email is not registered"
                    return;
                }else{
                    if(result){
                        console.log(result.email + "Email address");
                        var loginEmail = result.email
                        if(result.emailVerified == "true"){
                            console.log("email verified..")
                            return next();
                        }else{
                            console.log("email is not verified..")
                            
                            req.session.success = false;
                            req.session.emailNotVerified = true;
                            req.session.loginEmail = loginEmail;    
                            res.redirect('/recruiter/login');

                        }
                                               
                        // errors.push(error);                     
                    }else{
                        console.log("email is not registered..")
                    errormsg = []
                    errormsg.push({
                            msg: "Email is not registered.. "
                    });
                    req.session.errors = errormsg ;
                    req.session.success = false;
                    req.session.emailNotVerified = false;    
                    res.redirect('/recruiter/login');
                    }
                    
                    // Here need  to be next part of action - not outside find callback!

                }


                
            });
         }                    
        }) 
    } 
})
       
}


// Check Delete User
module.exports.deleteUser =function(req, res, next){
    recruiterControllers.checkValidUser(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            if(result){      
                //console.log(req.session.first_name);
                return next();
            }else{

                delete req.session.user_id;
                delete req.session.first_name;
                delete req.session.last_name;
                delete req.session.phone;
                delete req.session.username;
                delete req.session.user_role;
                delete req.session.image;
                res.redirect('/recruiter/login');
            }             
        }
    });    
}




// Frontend Authentication Checke
module.exports.frontendAuthentic =function(req, res, next){
    sessionValue=req.session;
    if(sessionValue.member_id){
        return next();
    }else{
        res.redirect('/login');
    }    
}


// Checke Member Login
module.exports.checkMemberLogin =function(req, res, next){
    sessionValue=req.session;
    if(sessionValue.member_id){
        res.redirect('/profile');
    }else{        
        return next();
    }    
}


// Check Delete Member
module.exports.deleteMember =function(req, res, next){

    homeControllers.getProfile(req, res, function(err,result){
        if(err){
            callback(err);
        }else{
            //console.log(result);
            if(result){      
                return next();
            }else{
                delete req.session.member_id;
                delete req.session.member_name;
                delete req.session.member_email;
                res.redirect('/login');
            }             
        }
    });    
}